package travelagency.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Trip dto
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class TripDto implements Serializable {

    protected static final long serialVersionUID = 1L;
    
    @EqualsAndHashCode.Include private Long id;

    private String name;

    private String description;

    private Date startDate;

    private Date endDate;

    private double price;

    private DestinationDto destination;
}
