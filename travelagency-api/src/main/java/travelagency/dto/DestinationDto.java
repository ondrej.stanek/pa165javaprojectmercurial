package travelagency.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Destination dto
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class DestinationDto implements Serializable {

    protected static final long serialVersionUID = 1L;
    
    @EqualsAndHashCode.Include protected Long id;

    private String name;

    private String country;
}
