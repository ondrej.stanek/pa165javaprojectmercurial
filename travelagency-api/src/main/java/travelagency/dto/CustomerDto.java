package travelagency.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Customer dto
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class CustomerDto implements Serializable {

    protected static final long serialVersionUID = 1L;
    
    @EqualsAndHashCode.Include protected Long id;

    private String name;

    private String email;

    private String phone;

    private String password;

    @Builder.Default private Set<ReservationDto> reservations = new HashSet<>();

    private Date born;

    private Date registered;
    
    private RoleDto role;
}
