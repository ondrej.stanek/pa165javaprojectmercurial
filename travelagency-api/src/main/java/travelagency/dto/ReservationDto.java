package travelagency.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Reservation dto
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class ReservationDto implements Serializable {

    protected static final long serialVersionUID = 1L;
    
    @EqualsAndHashCode.Include protected Long id;

    private CustomerDto customer;

    private TripDto trip;

    @Builder.Default private List<ExcursionDto> excursions = new ArrayList<>();

    private String comment;
}
