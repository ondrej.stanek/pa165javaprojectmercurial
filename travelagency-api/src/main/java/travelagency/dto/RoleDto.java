package travelagency.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Role dto
 *
 * @author Ondřej Staněk
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class RoleDto implements Serializable {
    
    protected static final long serialVersionUID = 1L;
    
    @EqualsAndHashCode.Include private Long id;
    
    private String role;
}
