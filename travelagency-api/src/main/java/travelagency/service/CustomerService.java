package travelagency.service;

import java.util.List;
import travelagency.dto.CustomerDto;

/**
 * Customer service
 * @author Boris Valo
 */
public interface CustomerService {

    /**
     * Adding of the customer
     *
     * @param customer customer
     * @return added customer
     */
    CustomerDto add(CustomerDto customer);

    /**
     * Updating the customer
     *
     * @param customer customer who is going to be updated
     * @return customer updated
     */
    CustomerDto update(CustomerDto customer);

    /**
     * Deleting the customer
     *
     * @param customer customer who is going to be deleted
     */
    void delete(CustomerDto customer);

    /**
     * Searching the customer by id
     *
     * @param id the id of customer, who is searched
     * @return customer with this id
     */
    CustomerDto getById(Long id);

    /**
     * Make the list of all customers
     *
     * @return list of the customers
     */
    List<CustomerDto> getAll();
    
    /**
     * Searching customers by email
     *
     * @param email the email of customers
     * @return customers with this email
     */
    List<CustomerDto> getByEmail(String email);
}
