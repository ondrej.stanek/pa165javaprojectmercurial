package travelagency.service;

import java.util.List;
import travelagency.dto.ExcursionDto;
import travelagency.dto.TripDto;

/**
 * Excursion service
 *
 * @author Boris Valo
 */
public interface ExcursionService {

    /**
     * Adding of the excursion
     *
     * @param excursion excursion
     * @return added excursion
     */
    ExcursionDto add(ExcursionDto excursion);

    /**
     * Updating the excursion
     *
     * @param excursion excursion which is going to be updated
     * @return excursion updated
     */
    ExcursionDto update(ExcursionDto excursion);

    /**
     * Deleting the excursion
     *
     * @param excursion excursion which is going to be deleted
     */
    void delete(ExcursionDto excursion);

    /**
     * Searching the excursion by id
     *
     * @param id the id of excursion, which is searched
     * @return excursion with this id
     */
    ExcursionDto getById(Long id);

    /**
     * Make the list of all excursions in the system
     *
     * @return list of the excursions
     */
    List<ExcursionDto> getAll();

    /**
     * Make the list of excursions connected to trip t
     *
     * @param t the trip which contains excursions
     * @return the list of all excursions of the trip t
     */
    List<ExcursionDto> getTripExcursions(TripDto t);
}
