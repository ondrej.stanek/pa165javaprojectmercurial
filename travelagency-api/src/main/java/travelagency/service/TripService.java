package travelagency.service;

import java.util.List;
import travelagency.dto.DestinationDto;
import travelagency.dto.TripDto;

/**
 * Trip service
 *
 * @author Boris Valo
 */
public interface TripService {

    /**
     * Adding of the trip
     *
     * @param trip trip
     * @return added trip
     */
    TripDto add(TripDto trip);

    /**
     * Updating the trip information
     *
     * @param trip trip which is going to be updated
     * @return trip updated
     */
    TripDto update(TripDto trip);

    /**
     * Deleting the trip with its excursions and reservations connected
     *
     * @param trip trip which is going to be deleted
     */
    void delete(TripDto trip);

    /**
     * Searching the trip by id
     *
     * @param id the id of trip, which is searched
     * @return trip with this id
     */
    TripDto getById(Long id);

    /**
     * Make the list of all trips
     *
     * @return list of the trips
     */
    List<TripDto> getAll();

    /**
     * Make the list of trips to destination d
     *
     * @param d the destination
     * @return the list of all trips to destination d
     */
    List<TripDto> getByDestination(DestinationDto d);
}
