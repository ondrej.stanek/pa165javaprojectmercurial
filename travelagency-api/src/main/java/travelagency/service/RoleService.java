package travelagency.service;

import java.util.List;
import travelagency.dto.RoleDto;

/**
 * Role service
 * 
 * @author Ondřej Staněk
 */
public interface RoleService {
   
    /**
     * Adding of the role
     *
     * @param role role
     * @return added role
     */
    RoleDto add(RoleDto role);

    /**
     * Updating the role
     *
     * @param role role which is going to be updated
     * @return role updated
     */
    RoleDto update(RoleDto role);

    /**
     * Deleting the role
     *
     * @param role role which is going to be deleted
     */
    void delete(RoleDto role);

    /**
     * Make the list of all roles
     *
     * @return list of the roles
     */
    List<RoleDto> getAll();
    
    /**
     * Searching roles by role name
     *
     * @param role the role name of roles
     * @return role with this role name
     */
    List<RoleDto> getByRole(String role);
}
