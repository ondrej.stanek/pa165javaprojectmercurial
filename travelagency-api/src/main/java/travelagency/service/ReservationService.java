package travelagency.service;

import java.util.List;
import travelagency.dto.ReservationDto;

/**
 * Reservation service
 *
 * @author Boris Valo
 */
public interface ReservationService {

    /**
     * Adding of the reservation
     *
     * @param reservation reservation
     * @return added reservation
     */
    ReservationDto add(ReservationDto reservation);

    /**
     * Updating the reservation
     *
     * @param reservation reservation which is going to be updated
     * @return reservation updated
     */
    ReservationDto update(ReservationDto reservation);

    /**
     * Deleting the reservation
     *
     * @param reservation reservation which is going to be deleted
     */
    void delete(ReservationDto reservation);

    /**
     * Searching the reservation by id
     *
     * @param id the id of reservation, which is searched
     * @return reservation with this id
     */
    ReservationDto getById(Long id);

    /**
     * Make the list of all reservations in the system
     *
     * @return list of the reservations
     */
    List<ReservationDto> getAll();

    /**
     * Searching the reservations which contains the string find in the name
     *
     * @param find the sequence of characters which are searched
     * @return the list of all reservations which contains string find
     */
    List<ReservationDto> findByString(String find);
}
