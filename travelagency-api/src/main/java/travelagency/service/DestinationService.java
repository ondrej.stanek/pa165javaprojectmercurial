package travelagency.service;

import java.util.List;
import travelagency.dto.DestinationDto;

/**
 * Destination service
 *
 * @author Boris Valo
 */
public interface DestinationService {

    /**
     * Adding of the destination
     *
     * @param destination destination
     * @return added destination
     */
    DestinationDto add(DestinationDto destination);

    /**
     * Updating the destination
     *
     * @param destination destination which is going to be updated
     * @return destination updated
     */
    DestinationDto update(DestinationDto destination);

    /**
     * Deleting the destination
     *
     * @param destination destination which is going to be deleted
     */
    void delete(DestinationDto destination);

    /**
     * Searching the destination by id
     *
     * @param id the id of destination, which is searched
     * @return destination with this id
     */
    DestinationDto getById(Long id);

    /**
     * Make the list of all destinations
     *
     * @return list of the destinations
     */
    List<DestinationDto> getAll();
}
