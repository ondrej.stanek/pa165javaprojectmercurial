# README #

Travel agency web application written in Java using Spring Framework, Maven, JPA, Hibernate, JUnit and Mockito.

### What is this repository for? ###

Application was written as a team project at the Faculty of Informatics at Masaryk University in 2014.
The team consisted of 4 people.


### How do I get set up? ###

Application compile and run requirements are (YOU HAVE TO CREATE AND START A NEW DATABASE!!! Instructions folow.):

- JDK 1.8

- JavaDB database (included in NetBeans IDE)

- for start from IDE - Tomcat version 9.0 application server (or version 8 or 7, beacause application uses libraries for Tomcat 7.0. Tomcat version 10 is not supported)

- Maven 3 (currently tested on version 3.6.3)


Database initialization:

- Create a database named pa165 with username pa165 and password pa165


Run the database before running the application on the application server. You can run application from NetBeans IDE.


---------------------------------WEB APPLICATION----------------------------------------------------

To set up correct database configuration run sql statements below in your database
before first run of the appliaction:
`CREATE TABLE ROLE (ID BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
                   ROLE VARCHAR(255));
INSERT INTO ROLE (ROLE) VALUES ('ROLE_ADMIN');
INSERT INTO ROLE (ROLE) VALUES ('ROLE_USER');`

### How do I run the application? ###

To run web application switch to travelagency-web folder using `cd travelagency-web`
and execute `mvn tomcat7:run` statement.

The application will be available on http://localhost:8080/pa165/ website.

To create an administrator account you can use an auxiliary administrator account
with username: `admin`, password: `admin`.
Then you can change role of any user from list of users.


----------------------------------REST API CLIENT---------------------------------------------------

Before running and testing API you have to run web application.
To run client switch to travelagency-cli folder using `cd travelagency-cli`.

Run the following command for client application help: `mvn exec:java -Dexec.args="-help"`

The client is made for testing Customer and Destination entities.
You can test this actions: `add`, `update`, `delete`, `show one`, `show all`.

Usage: `mvn exec:java -Dexec.args="url operation entity [id]"`

Parameters:

    1. URL of API 
	2. Action (add, update, delete, show, list)
	3. Entity (customer, destination)
	4. ID - required just for some actions (delete, show)

Actions:

	add - This action allows add new entity. Aplication will ask for all data.
		Example to start: mvn exec:java -Dexec.args="http://localhost:8080/pa165/rest add customer"
		
	update - This action allows edit some exists entity. ID of entity you will enter after start.
		Example to start: mvn exec:java -Dexec.args="http://localhost:8080/pa165/rest update customer"

	show - This action returns one entity of given ID.
		Example to start: mvn exec:java -Dexec.args="http://localhost:8080/pa165/rest show customer 1"

	list - This action shows all entites of one type.
		Example to start: mvn exec:java -Dexec.args="http://localhost:8080/pa165/rest list customer"

	delete - This action delete entity from database.
		Example to start: mvn exec:java -Dexec.args="http://localhost:8080/pa165/rest delete customer 1"
