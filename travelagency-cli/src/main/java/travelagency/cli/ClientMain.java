package travelagency.cli;

import com.google.common.primitives.Ints;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import travelagency.dto.CustomerDto;
import travelagency.dto.DestinationDto;
import travelagency.restclient.CustomerRestClient;
import travelagency.restclient.DestinationRestClient;
import travelagency.restclient.RestClientInterface;

/**
 *
 * @author Jan Střálka
 */
@Slf4j
public class ClientMain {

    /**
     * Parameters: server operation entity [id]
     * For example: http://localhost:8084/pa165/rest delete customer 1
     *
     * @param args application arguments
     */
    public static void main(String[] args) {

        Options options = new Options();
        options.addOption("help", false, "");
        
        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        
        try {
            line = parser.parse(options, args);
        }
        catch(ParseException e) {
            log.error("Parsing client application arguments failed. Reason:", e);
            return;
        }
        
        if (args.length == 0 || line.hasOption("help")) {
            final PrintWriter writer = new PrintWriter(System.out);
            writer.println("Travel Agency command line client");
            writer.println("USAGE:");
            writer.println("    mvn exec:java -Dexec.args=\"url operation entity [id]\"");
            writer.println();
            writer.println("where");
            writer.println("    url           Valid base URL of REST server");
            writer.println("    operation     Supported operations are:");
            writer.println("                    add - adds item");
            writer.println("                    update - updates item");
            writer.println("                    delete - removes item by ID");
            writer.println("                    show - shows item by ID");
            writer.println("                    list - shows all items");
            writer.println("    entity        Supported entities are:");
            writer.println("                    customer");
            writer.println("                    destination");
            writer.println("    id            ID of item for selected operation");
            writer.flush();
            return;
        }

        if (args.length < 3) {
            System.err.println("Invalid argument");
            return;
        }

        String baseUrl = args[0];
        
        if(!isApiAvailable(baseUrl)){
            System.err.println("Server and API is not available. Please, run the server with API or get right URL");
            return;
        }

        String argCmd = args[1].toLowerCase();
        ClientOperation op;
        switch (argCmd) {
            case "add":
                op = ClientOperation.ADD;
                break;
            case "delete":
                op = ClientOperation.DELETE;
                break;
            case "update":
                op = ClientOperation.UPDATE;
                break;
            case "list":
                op = ClientOperation.LIST;
                break;
            case "show":
                op = ClientOperation.SHOW;
                break;
            default:
                System.err.println("Invalid operation");
                return;
        }

        String entity = args[2].toLowerCase();
        RestClientInterface restClient = getClientByEntity(entity, baseUrl);
        if (restClient == null) {
            return;
        }
        Long id;

        switch (op) {
            case ADD:
                Object dto1 = getDtoByEntity(entity);
                if (dto1 == null) {
                    return;
                }
                fillEntityFromInput(dto1, false, restClient);
                restClient.add(dto1);
                break;
            case UPDATE:
                Object dto2 = getDtoByEntity(entity);
                if (dto2 == null) {
                    return;
                }
                fillEntityFromInput(dto2, true, restClient);
                restClient.update(dto2);
                break;
            case DELETE:
                if (args.length < 4) {
                    System.err.println("Identifier argument expected");
                    return;
                }
                try {
                    id = Long.parseLong(args[3]);
                } catch (Exception e) {
                    System.err.println("Identifier argument expected");
                    return;
                }

                restClient.delete(id);
                break;
            case LIST:
                List<Object> dtoList = restClient.getAll();
                for (Object ent : dtoList) {
                    showEntity(ent);
                    System.out.println("------------------------------------------------------------");
                }
                break;
            case SHOW:
                if (args.length < 4) {
                    System.err.println("Identifier argument expected");
                    return;
                }
                try {
                    id = Long.parseLong(args[3]);
                } catch (Exception e) {
                    System.err.println("Identifier argument expected");
                    return;
                }
                Object dto = restClient.getById(id);
                showEntity(dto);
                break;
            default:
                System.err.println("Invalid operation");
        }
    }

    /**
     * Choosing the entity which we want to create
     *
     * @param entity parameter could be "customer" or "destination"
     * @return new customer, destination or error message
     */
    public static Object getDtoByEntity(String entity) {
        switch (entity) {
            case "customer":
                return new CustomerDto();
            case "destination":
                return new DestinationDto();
            default:
                System.err.println("Wrong entity type");
                return null;
        }
    }

    /**
     * Rest client interface
     *
     * @param entity parameter coul be "customer" or "destination"
     * @param baseUrl base url
     * @return
     */
    public static RestClientInterface getClientByEntity(String entity, String baseUrl) {
        switch (entity) {
            case "customer":
                return new CustomerRestClient(baseUrl);
            case "destination":
                return new DestinationRestClient(baseUrl);
            default:
                System.err.println("Wrong entity type");
                return null;
        }
    }

    /**
     * Fill entity from input
     *
     * @param dto object dto
     * @param retrieveId id retrieved
     * @param client client
     */
    public static void fillEntityFromInput(Object dto, boolean retrieveId, RestClientInterface client) {
        try {
            if (dto instanceof CustomerDto) {
                CustomerDto customerDto = (CustomerDto) dto;
                if (retrieveId) {
                    Long id = inputNumeric("Enter ID of existing customer");
                    customerDto.setId(id);
                    CustomerDto dtoTemp = (CustomerDto) client.getById(id);
                    if (dtoTemp == null || dtoTemp.getId() == null) {
                        throw new RuntimeException("Destination on given ID was not found.");
                    }
                    System.out.println("------------------------------------------------------------");
                    System.out.println("Edited Customer:");
                    showEntity(client.getById(id));
                    System.out.println("------------------------------------------------------------");
                }
                customerDto.setName(inputString("Enter name"));
                customerDto.setEmail(inputString("Enter e-mail"));
                customerDto.setPhone(inputString("Enter phone"));
                customerDto.setPassword(inputString("Enter password"));
                int bornYear = Ints.checkedCast(ClientMain.inputNumeric("Enter born year", 1900L, 1900L + (new Date()).getYear()));
                int bornMonth = Ints.checkedCast(ClientMain.inputNumeric("Enter born month [1-12]", 1L, 12L));
                int bornDate = Ints.checkedCast(ClientMain.inputNumeric("Enter born date [1-31]", 1L, 31L));
                customerDto.setBorn(new Date(bornYear - 1900, bornMonth - 1, bornDate));
                customerDto.setRegistered(new Date());
            } else if (dto instanceof DestinationDto) {
                DestinationDto destinationDto = (DestinationDto) dto;
                if (retrieveId) {
                    Long id = inputNumeric("Enter ID of existing destination");
                    destinationDto.setId(id);
                    DestinationDto dtoTemp = (DestinationDto) client.getById(id);
                    if (dtoTemp == null || dtoTemp.getId() == null) {
                        throw new RuntimeException("Destination on given ID was not found.");
                    }
                    System.out.println("------------------------------------------------------------");
                    System.out.println("Edited Destination:");
                    showEntity(dtoTemp);
                    System.out.println("------------------------------------------------------------");
                }
                destinationDto.setName(inputString("Enter name"));
                destinationDto.setCountry(inputString("Enter country"));
            } else {
                throw new RuntimeException("Unknown DTO type");
            }
        } catch (Exception e) {
            log.error("Error:", e);
        }

    }

    /**
     * Show entity by giving object
     *
     * @param dto dto object
     */
    public static void showEntity(Object dto) {
        DateFormat dateFormat = DateFormat.getDateInstance();
        if (dto instanceof CustomerDto) {
            CustomerDto customerDto = (CustomerDto) dto;
            System.out.println("ID:          " + customerDto.getId());
            System.out.println("Name:        " + customerDto.getName());
            System.out.println("E-mail:      " + customerDto.getEmail());
            System.out.println("Phone:       " + customerDto.getPhone());
            System.out.println("Password:    " + customerDto.getPassword());
            System.out.println("Born:        " + dateFormat.format(customerDto.getBorn()));
            System.out.println("Registered:  " + dateFormat.format(customerDto.getRegistered()));
        } else if (dto instanceof DestinationDto) {
            DestinationDto destinationDto = (DestinationDto) dto;
            System.out.println("ID:          " + destinationDto.getId());
            System.out.println("Name:        " + destinationDto.getName());
            System.out.println("Country:     " + destinationDto.getCountry());
        }
    }

    /**
     * Input string
     * @param label name of the string input
     * @return line of text
     * @throws IOException 
     */
    public static String inputString(String label) throws IOException {
        System.out.print(label + ": ");
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        String line = buffer.readLine();
        return line;
    }

    /**
     * Input number
     * @param label name 
     * @param min minimal value
     * @param max maximal value
     * @return number
     * @throws IOException 
     */
    public static Long inputNumeric(String label, Long min, Long max) throws IOException {
        Long num = 0L;
        boolean parsed = false;
        do {
            parsed = true;
            String numStr = inputString(label);
            try {
                num = Long.parseLong(numStr);
                if (num < min || num > max) {
                    throw new Exception();
                }
            } catch (Exception e) {
                parsed = false;
                System.err.println("Please enter positive numeric value in range " + min + " - " + max);
            }
        } while (!parsed);
        return num;
    }

    /**
     * Input number without range
     * @param label name of the number input
     * @return inputNumeric
     * @throws IOException 
     */
    public static Long inputNumeric(String label) throws IOException {
        return inputNumeric(label, 0L, Long.MAX_VALUE);
    }

    private static boolean isApiAvailable(String url) {
        Pattern pattern = Pattern.compile("(http?://)([^:^/]*)(:\\d*)?(.*)?");
        Matcher matcher = pattern.matcher(url);

        matcher.find();

        String protocol = matcher.group(1);            
        String domain   = matcher.group(2);
        String port     = matcher.group(3).replace(":", "");
        String uri      = matcher.group(4);
        
        try (Socket ignored = new Socket(domain, Integer.parseInt(port))) {
            return true;
        } catch (IOException ioe) {
            return false;
        }
    }
}
