package travelagency.cli;

/**
 * Client operations
 *
 * @author Jan Střálka
 */
public enum ClientOperation {

    LIST,
    SHOW,
    ADD,
    UPDATE,
    DELETE,

}
