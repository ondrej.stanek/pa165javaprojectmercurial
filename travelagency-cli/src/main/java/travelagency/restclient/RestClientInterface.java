package travelagency.restclient;

import java.util.List;

/**
 * Rest Client interface
 *
 * @author Jan Střálka
 */
public interface RestClientInterface<T> {

    /**
     * Shows all of the entities
     *
     * @return the list of all entities
     */
    public List<T> getAll();

    /**
     * Search the entity by id
     *
     * @param id of entity we are searching
     * @return entity with this id
     */
    public T getById(Long id);

    /**
     * Adding the dto
     *
     * @param dto dto added
     */
    public void add(T dto);

    /**
     * Updating the dto
     *
     * @param dto updated
     */
    public void update(T dto);

    /**
     * Deleting of the entity by id
     *
     * @param id of entity we want to delete
     */
    public void delete(Long id);
}
