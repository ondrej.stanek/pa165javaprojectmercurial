package travelagency.restclient;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import travelagency.dto.DestinationDto;

/**
 * Destination rest client
 *
 * @author Jan Střálka
 */
public class DestinationRestClient implements RestClientInterface {

    private final Client client;
    private final WebTarget target;
    private final Genson genson;

    /**
     * Destination rest client
     *
     * @param url url
     */
    public DestinationRestClient(String url) {
        this.client = ClientBuilder.newBuilder().register(MoxyJsonFeature.class).build();
        this.target = this.client.target(url);
        this.genson = new GensonBuilder().useDateAsTimestamp(true).create();
    }

    @Override
    public List<DestinationDto> getAll() {
        WebTarget myTarget = this.target.path("destinations");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationbuilder.accept(MediaType.APPLICATION_JSON_TYPE).get();

        List<Object> destinationHashMaps = this.genson.deserialize(response.readEntity(String.class), List.class);
        List<DestinationDto> destinations = new ArrayList<>();
        for (Object hm : destinationHashMaps)
        {
            DestinationDto destination = this.genson.deserialize(this.genson.serialize(hm), DestinationDto.class);
            destinations.add(destination);
        }
        return destinations;
    }

    @Override
    public DestinationDto getById(Long id) {
        WebTarget myTarget = this.target.path("destination").path(id.toString());
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationbuilder.accept(MediaType.APPLICATION_JSON_TYPE).get();

        DestinationDto destination = this.genson.deserialize(response.readEntity(String.class), DestinationDto.class);
        return destination; 
    }

    @Override
    public void add(Object dto) {
        WebTarget myTarget = this.target.path("destination").path("add");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        DestinationDto posted = invocationbuilder.post(Entity.entity(dto, MediaType.APPLICATION_JSON), DestinationDto.class);
        return;
    }

    @Override
    public void update(Object dto) {
        WebTarget myTarget = this.target.path("destination").path("update");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        DestinationDto posted = invocationbuilder.post(Entity.entity(dto, MediaType.APPLICATION_JSON), DestinationDto.class);
        return;
    }

    @Override
    public void delete(Long id) {
        try {
            WebTarget myTarget = this.target.path("destination").path("delete").path(id.toString());
            Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
            Response response = invocationbuilder.get();
            if (response.getStatus() == 200) {
                System.out.println("Destination was sucsessfully deleted");
            } else {
                throw new RuntimeException();
            }
        } catch (Exception e) {
            System.err.println("Error caused while deleting of destination");
        }
    }

}
