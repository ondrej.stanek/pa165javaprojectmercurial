package travelagency.restclient;

import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import travelagency.dto.CustomerDto;
import com.owlike.genson.*;
import java.util.ArrayList;

/**
 * Customer rest client
 *
 * @author Jan Střálka
 */
public class CustomerRestClient implements RestClientInterface<CustomerDto> {
    
    private final Client client;
    private final WebTarget target;
    private final Genson genson;

    /**
     * Customer rest client
     * @param url url
     */
    public CustomerRestClient(String url) {
        this.client = ClientBuilder.newBuilder().register(MoxyJsonFeature.class).build();
        this.target = this.client.target(url);
        this.genson = new GensonBuilder().useDateAsTimestamp(true).create();
    }

    @Override
    public List<CustomerDto> getAll() {
        WebTarget myTarget = this.target.path("customers");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationbuilder.accept(MediaType.APPLICATION_JSON_TYPE).get();

        List<Object> customerHashMaps = this.genson.deserialize(response.readEntity(String.class), List.class);
        List<CustomerDto> customers = new ArrayList<>();
        for (Object hm : customerHashMaps)
        {
            CustomerDto customer = this.genson.deserialize(this.genson.serialize(hm), CustomerDto.class);
            customers.add(customer);
        }
        return customers;
    }

    @Override
    public CustomerDto getById(Long id) {
        WebTarget myTarget = this.target.path("customer").path(id.toString());
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationbuilder.accept(MediaType.APPLICATION_JSON_TYPE).get();

        CustomerDto customer = this.genson.deserialize(response.readEntity(String.class), CustomerDto.class);
        return customer; 
    }

    @Override
    public void add(CustomerDto dto) {
        WebTarget myTarget = this.target.path("customer").path("add");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        CustomerDto posted = invocationbuilder.post(Entity.entity(dto, MediaType.APPLICATION_JSON), CustomerDto.class);
        return;
    }

    @Override
    public void update(CustomerDto dto) {
        WebTarget myTarget = this.target.path("customer").path("update");
        Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
        CustomerDto posted = invocationbuilder.post(Entity.entity(dto, MediaType.APPLICATION_JSON), CustomerDto.class);
        return;
    }

    @Override
    public void delete(Long id) {
        try {
            WebTarget myTarget = this.target.path("customer").path("delete").path(id.toString());
            Invocation.Builder invocationbuilder = myTarget.request(MediaType.APPLICATION_JSON);
            Response response = invocationbuilder.get();
            if (response.getStatus() == 200) {
                System.out.println("Customer was sucsessfully deleted");
            } else {
                throw new RuntimeException();
            }
        } catch (Exception e) {
            System.err.println("Error caused while deleting of customer");
        }
    }
}
