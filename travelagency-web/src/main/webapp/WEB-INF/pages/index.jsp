<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:attribute name="title">
        <f:message key="index.title"/>
    </jsp:attribute>
    <jsp:body>
        <f:setLocale value="${language}" />
        <f:setBundle basename="Texts" />
        <div class="row">
            <div class="col-md-12">
                <div class="trip">
                    <h2><f:message key="index.title" /></h2>
                    <p class="description"><f:message key="index.description"/>
                    </p>    
                    <br />
                    <p class="description"><f:message key="index.description2"/>
                    </p>
                </div>        
            </div>  
        </div>
    </jsp:body>
</t:layout>
