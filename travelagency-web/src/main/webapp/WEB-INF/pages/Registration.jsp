<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<my:layout>
    <jsp:attribute name="title">
        <f:message key="registration.title"/>
    </jsp:attribute>
    <jsp:body>
        <h1><f:message key="registration.title"/></h1>
        <div class="trip trip-detail">
            <c:if test="${registerSuccess}">
                <h3><f:message key="registration.success"/></h3>
            </c:if>
            <h3><f:message key="registration.fill"/></h3>
            <form:form commandName="registerUserForm" action="${pageContext.request.contextPath}/registration">
                <form:errors path="*" cssClass="errorblock" element="p" />
                <p class="names">
                    <form:label path="name"><f:message key="registration.name"/></form:label> <form:input path="name" type="text" /><br />
                    <form:label path="email"><f:message key="registration.email"/></form:label> <form:input path="email" type="text" /><br />
                    <form:label path="password"><f:message key="registration.password"/></form:label> <form:password path="password" /><br />
                    <form:label path="passwordAgain"><f:message key="registration.passwordAgain"/></form:label> <form:password path="passwordAgain" />
                    </p>
                    <p class="names">
                        <form:label path="phone"><f:message key="registration.phone"/></form:label> <form:input path="phone" type="text" />
                    </p>
                    <p class="names">
                    </p>
                <form:button class="submitButton" type="submit"><f:message key="register.submit" /></form:button>
            </form:form>
        </div>
    </jsp:body>
</my:layout>
