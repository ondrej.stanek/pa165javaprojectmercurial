<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="reservation.title"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="reservation.title"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <sec:authorize ifAllGranted="ROLE_ADMIN">
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="navigation.trips"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                        </sec:authorize>
                    </ul>
                </div>
                <div class="col-md-10">
                    <form method="get" action="${pageContext.request.contextPath}/reservations">
                        <div class="input-group">
                            <input id="reservations_find" type="text" name="find" value="<c:out value="${formStr}" />" class="form-control" />
                            <div class="input-group-btn">
                                <input type="submit" value="Hledat" class="btn btn-success" />
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th><f:message key="trip.title"/></th>
                            <th><f:message key="reservation.excursions"/></th>		
                            <th><f:message key="reservation.date"/></th>
                            <th><f:message key="reservation.price"/></th>
                            <th><f:message key="reservation.action"/></th>
                        </tr>
                        <c:forEach items="${reservations}" var="reservation">
                        <tr>
                            <td><c:out value="${reservation.getTrip().getName()}" /></td>
                            <td><c:forEach items="${reservation.getExcursions()}" var="exc"><c:out value="${exc.getName()}" /><br /> </c:forEach></td>		
                            <td>${reservation.getTrip().getStartDate().getDate()}. ${reservation.getTrip().getStartDate().getMonth()+1}. ${reservation.getTrip().getStartDate().getYear()+1900}
                                    - ${reservation.getTrip().getEndDate().getDate()}. ${reservation.getTrip().getEndDate().getMonth()+1}. ${reservation.getTrip().getEndDate().getYear()+1900}</td>
                            <td>${reservation.getTrip().getPrice()} &euro;</td>	
                            <td>
                                <a href="${pageContext.request.contextPath}/reservation/edit/${reservation.getId()}"><f:message key="general.edit"/></a>
                                <a href="${pageContext.request.contextPath}/reservation/delete/${reservation.getId()}"><f:message key="general.delete"/></a>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>               
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>