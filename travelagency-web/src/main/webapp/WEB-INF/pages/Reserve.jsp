<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="reservation.title"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1>Nová rezervace</h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <sec:authorize ifAllGranted="ROLE_ADMIN">
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="navigation.trips"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                        </sec:authorize>
                    </ul>
                </div>
                <div class="col-md-10">
                    
                    <form:form commandName="reserveForm" action="${pageContext.request.contextPath}/reservation/new/done">
                        <table>
                            <tr><td><form:label path="customer">Uživatel</form:label></td><td><form:select path="customer" items="${users}" /></td></tr>
                            <tr><td><form:label path="comment">Komentář</form:label></td><td><form:textarea path="description" rows="5" cols="30" /></td></tr>
                            <tr>
                                <td colspan="2">
                                    <form:hidden path="trip"/>
                                    <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </div>
            </div
        </div>
    </jsp:body>
</t:layout>