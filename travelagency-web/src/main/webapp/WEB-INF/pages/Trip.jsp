<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="t" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ page contentType="text/html; charset=UTF-8" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="trip.title"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="trip.title"/></h1>
                <div class="col-md-2">
                    <c:choose>
                        <c:when test="${menuType == 'dest'}">
                        <ul class="nav nav-pills nav-stacked">
                            <c:forEach items="${destinations}" var="dest">
                            <li><a href="${pageContext.request.contextPath}/trips/${dest.getId()}">${dest.getName()}<br />(${dest.getCountry()})</a></li>
                            </c:forEach>
                        </ul>
                        </c:when>
                        <c:otherwise>
                        <ul class="nav nav-pills nav-stacked">
                            <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                            <sec:authorize ifAllGranted="ROLE_ADMIN">
                            <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="navigation.trips"/></a></li>
                            <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                            <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                            </sec:authorize>
                        </ul>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-10">
                    <c:if test="${saveSuccess}">
                        <h3>Rezervace byla uložena</h3>
                    </c:if>
                    <div class="trip trip-detail">
                        <h2><a href="${pageContext.request.contextPath}/trip/1"><c:out value="${trip.getName()}" /></a></h2>
                        <p class="description"><c:out value="${trip.getDescription()}" /></p>            
                        <p class="term">${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()+1}. ${trip.getStartDate().getYear()+1900}
                                - ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()+1}. ${trip.getEndDate().getYear()+1900}</p>
                        <form:form commandName="reserveForm" action="${pageContext.request.contextPath}/reservation/done">
                            <table>
                                <tr><td><form:label path="customer">Uživatel</form:label></td><td><form:select path="customer" items="${users}" /></td></tr>
                                <tr><td><form:label path="comment"><f:message key="general.note"/></form:label></td><td><form:textarea path="comment" rows="5" cols="30" /></td></tr>
                                <tr>
                                    <td><form:label path="excursions">Exkurze</form:label></td>
                                    <td>
                                        <ul>
                                        <form:checkboxes element="li" path="excursions" items="${excursions}"></form:checkboxes>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <form:hidden path="trip"/>
                                        <form:hidden path="reservationId"/>
                                        <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                    </td>
                                </tr>
                            </table>
                        </form:form>    
                    </div>  
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>