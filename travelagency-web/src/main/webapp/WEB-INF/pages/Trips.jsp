<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="trips.title"/>
    </jsp:attribute>
    <jsp:body>
    <div class="row">
        <div id="content-with-menu" class="col-md-12">
            <h1><f:message key="trips.title"/></h1>
                <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                        <c:forEach items="${destinations}" var="dest">
                        <li <c:if test="${active == dest.getId()}">class="active"</c:if>><a href="${pageContext.request.contextPath}/trips/${dest.getId()}">${dest.getName()}<br />(${dest.getCountry()})</a></li>
                        </c:forEach>
                    </ul>
                </div>
                <div class="col-md-9">
                    <c:forEach items="${trips}" var="trip">
                        <div class="trip">
                            <h2><a href="${pageContext.request.contextPath}/reservation/new/${trip.getId()}"><c:out value="${trip.getName()}" /></a></h2>
                            <p class="description"><c:out value="${trip.getDescription()}" /></p>            
                            <p class="term">${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()+1}. ${trip.getStartDate().getYear()+1900}
                                - ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()+1}. ${trip.getEndDate().getYear()+1900}</p>
                                <div class="priceAndregister">
                                    <span>Cena: ${trip.getPrice()} &euro;</span>
                                <a href="${pageContext.request.contextPath}/reservation/new/${trip.getId()}"><f:message key="general.reserve"/></a>
                                </div>
                        </div>
                    </c:forEach>
                </div>            
        </div>
    </div>      
    </jsp:body>
</t:layout>
