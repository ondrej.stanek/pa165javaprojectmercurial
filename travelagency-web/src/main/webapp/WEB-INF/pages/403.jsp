<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<!DOCTYPE html>
<t:layout>
    <jsp:attribute name="title">
        <f:message key="login.title"/>
    </jsp:attribute>
    <jsp:body>
        <div id="content">
            <h2>HTTP Status 403 - Nemáte oprávnění k přístupu na stránku.</h2>
            <h2>${msg}</h2>
        </div>
    </jsp:body>
</t:layout>
