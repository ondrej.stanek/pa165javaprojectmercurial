<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<%@page session="true"%>

<t:layout>
    <jsp:attribute name="title">
        <f:message key="login.title"/>
    </jsp:attribute>
    <jsp:body>
        <h1><f:message key="login.title"/></h1>
        <div class="trip trip-detail">
            <form name="loginForm" method="post" action="<c:url value="${pageContext.request.contextPath}/j_spring_security_check" />">
                <c:if test="${error == true}">
                    <strong>Chyba:</strong> Nesprávný e-mail nebo heslo.
                </c:if>
                <table>
                    <tr><td><label for="login_email"><f:message key="registration.email"/></label></td><td><input type="text" name="j_username" id="j_username" /></td></tr>
                    <tr><td><label for="login_password"><f:message key="registration.password"/></label></td><td><input type="password" name="j_password" id="j_password" /></td></tr>
                    <tr><td colspan="2"><input class="submitButton" id="submit" type="submit" name="submit" value="<f:message key="login.submit"/>"/></td></tr>
                </table>
                
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                
            </form>
        </div>
    </jsp:body>
</t:layout>