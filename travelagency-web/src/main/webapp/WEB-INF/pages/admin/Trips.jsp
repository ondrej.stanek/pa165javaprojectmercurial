<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="trip.title"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="trip.admin"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="trips.title"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <a href="${pageContext.request.contextPath}/admin/trip/create" class="btn btn-primary"><f:message key="trip.add"/></a>
                    </div>
                    <div class="row">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <th><f:message key="admin.name"/></th>
                                <th><f:message key="excursion.description"/></th>
                                <th><f:message key="reservation.date"/></th>
                                <th><f:message key="reservation.price"/></th>
                                <th><f:message key="reservation.action"/></th>
                            </tr>
                            <c:forEach items="${trips}" var="trip">
                                <tr>
                                    <td>${trip.getName()}</td>
                                    <td>${trip.getDestination().getName()}</td>
                                    <td>${trip.getStartDate().getDate()}. ${trip.getStartDate().getMonth()+1}. ${trip.getStartDate().getYear()+1900}
                                        - ${trip.getEndDate().getDate()}. ${trip.getEndDate().getMonth()+1}. ${trip.getEndDate().getYear()+1900}</td>
                                    <td>${trip.getPrice()} &euro;</td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/admin/trip/edit/${trip.getId()}"><f:message key="general.edit"/></a>
                                        <a href="${pageContext.request.contextPath}/admin/trip/delete/${trip.getId()}"><f:message key="general.delete"/></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>