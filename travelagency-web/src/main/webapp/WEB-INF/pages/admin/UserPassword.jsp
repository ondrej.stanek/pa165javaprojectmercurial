<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="password.edit"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="password.edit"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="trips.title"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    
                    <form:form commandName="passForm" action="${pageContext.request.contextPath}/admin/user/password/change">
                        <c:if test="${saveSuccess}">
                            <h3><f:message key="password.saved"/></h3>
                        </c:if>
                        <table>
                            <tr><td><form:label path="password"><f:message key="registration.password"/></form:label></td><td><form:input path="password" type="password" /></td></tr>
                            <tr><td><form:label path="passwordAgain"><f:message key="registration.passwordAgain"/></form:label></td><td><form:input path="passwordAgain" type="password" /></td></tr>
                            <tr>
                                <td colspan="2">
                                    <form:hidden path="id"/>
                                    <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                </td>
                            </tr>
                        </table>
                    </form:form>
                </div>
            </div
        </div>
    </jsp:body>
</t:layout>