<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="trip.edit"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="trip.edit"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="trips.title"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                        <form:form commandName="tripForm" method="post" action="${pageContext.request.contextPath}/admin/trip/create">
                            <form:errors path="*" cssClass="errorblock" element="p" />
                            <c:if test="${saveSuccess}">
                                <h3><f:message key="country.saved"/></h3>
                            </c:if>
                            <table>
                                <tr><td><form:label path="name"><f:message key="admin.name"/></form:label></td><td><form:input path="name" type="text" /></td></tr>
                                <tr><td><form:label path="description"><f:message key="trip.description"/></form:label></td><td><form:textarea path="description" rows="5" cols="30" /></td></tr>
                                <tr><td><form:label path="startDate"><f:message key="trip.startDate"/>Od</form:label></td><td><form:input type="text" path="startDate" class="datepicker" /></td>
                                    <td><form:label path="endDate"><f:message key="trip.endDate"/></form:label></td><td><form:input type="text" path="endDate" class="datepicker" /></td></tr>
                                <tr><td><form:label path="destination"><f:message key="reservation.destinations"/></form:label></td><td><form:select path="destination" items="${destinations}" /></td></tr>
                                <tr><td><form:label path="price"><f:message key="reservation.price"/></form:label></td><td><form:input path="price" type="number" /></td></tr>
                                <tr>
                                    <td colspan="2">
                                        <form:hidden path="id"/>
                                        <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                    </td>
                                </tr>
                            </table>


                        </form:form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><f:message key="trip.excursionsList"/></h3>
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th><f:message key="admin.name"/></th>
                                    <th><f:message key="excursion.description"/></th>
                                    <th><f:message key="reservation.date"/></th>
                                    <th><f:message key="reservation.price"/></th>
                                    <th><f:message key="reservation.action"/></th>
                                </tr>
                                <c:forEach items="${excursions}" var="exc">
                                <tr>
                                    <td>${exc.getName()}</td>
                                    <td>${exc.getDescription()}</td>
                                    <td>${exc.getStartDate().getDate()}. ${exc.getStartDate().getMonth()+1}. ${exc.getStartDate().getYear()+1900}
                                        - ${exc.getEndDate().getDate()}. ${exc.getEndDate().getMonth()+1}. ${exc.getEndDate().getYear()+1900}</td>
                                    <td>${exc.getPrice()}</td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/admin/excursion/delete/${exc.getId()}"><f:message key="general.delete"/></a>
                                    </td>
                                </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h3><f:message key="excursion.new"/></h3>
                        <form:form commandName="excursionForm" method="post" action="${pageContext.request.contextPath}/admin/excursion/create">
                            <form:errors path="*" cssClass="errorblock" element="p" />
                            <table>
                                 <tr><td><form:label path="name"><f:message key="admin.name"/></form:label></td><td><form:input path="name" type="text" /></td></tr>
                                <tr><td><form:label path="description"><f:message key="trip.description"/></form:label></td><td><form:textarea path="description" rows="5" cols="30" /></td></tr>
                                <tr><td><label for="exc_startDate"><f:message key="trip.startDate"/></label></td><td><form:input type="text" path="startDate" id="exc_startDate" class="datepicker" /></td>
                                    <td><label for="exc_endDate"><f:message key="trip.endDate"/></label></td><td><form:input type="text" path="endDate" id="exc_endDate" class="datepicker" /></td></tr>
                                <tr><td><form:label path="price"><f:message key="reservation.price"/></form:label></td><td><form:input path="price" type="number" /></td></tr>
                                <tr>
                                    <td colspan="2">
                                        <form:hidden path="trip"/>
                                        <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                    </td>
                                </tr>
                            </table>


                        </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>