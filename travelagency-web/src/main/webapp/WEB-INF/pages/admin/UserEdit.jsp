<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="user.edit"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
          <div id="content">
            <h1><f:message key="user.edit"/></h1>
            <div class="col-md-2">
                 <ul class="nav nav-pills nav-stacked">
                    <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations">Rezervace</a></li>
                    <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips">Zájezdy</a></li>
                    <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users">Uživatelé</a></li>
                    <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations">Destinace</a></li>
                </ul>
            </div>
            <div class="col-md-10">
              <form:form commandName="userForm" action="${pageContext.request.contextPath}/admin/users">
                <table>
                    <tr><td><form:label path="name"><f:message key="general.name"/></form:label></td><td><form:input type="text" path="name" /></td></tr>
                    <tr><td><form:label path="email"><f:message key="registration.email"/></form:label></td><td><form:input type="text" path="email" /></td></tr>
                    <tr><td><form:label path="phone"><f:message key="registration.phone"/></form:label></td><td><form:input type="text" path="phone" /></td></tr>
                    <tr>
                        <td colspan="2">
                            <form:hidden path="id"/>
                            <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                        </td>
                    </tr>
                </table>
              </form:form>
            </div>
        </div>
        </div>
    </jsp:body>
</t:layout>