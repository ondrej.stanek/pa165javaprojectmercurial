<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="user.list"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="user.list"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="trips.title"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th><f:message key="general.name"/></th>
                            <th><f:message key="reservation.email"/></th>
                            <th><f:message key="reservation.phone"/></th>
                            <th><f:message key="reservation.action"/></th>
                        </tr>
                        <c:forEach items="${users}" var="user">
                        <tr>
                            <td><c:out value="${user.getName()}" /></td>
                            <td><c:out value="${user.getEmail()}" /></td>
                            <td><c:out value="${user.getPhone()}" /></td>
                            <td>
                                <a href="${pageContext.request.contextPath}/admin/user/edit/${user.getId()}"><f:message key="general.edit"/></a>
                                <a href="${pageContext.request.contextPath}/admin/user/password/${user.getId()}"><f:message key="password.edit"/></a>
                                <a href="${pageContext.request.contextPath}/admin/user/delete/${user.getId()}"><f:message key="general.delete"/></a>
                                <c:if test="${user.getRole().getRole() != 'ROLE_ADMIN'}"><a href="${pageContext.request.contextPath}/admin/user/setadmin/${user.getId()}"><f:message key="general.setadmin"/></a></c:if>
                                <c:if test="${user.getRole().getRole() == 'ROLE_ADMIN'}"><a href="${pageContext.request.contextPath}/admin/user/unsetadmin/${user.getId()}"><f:message key="general.unsetadmin"/></a></c:if>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>