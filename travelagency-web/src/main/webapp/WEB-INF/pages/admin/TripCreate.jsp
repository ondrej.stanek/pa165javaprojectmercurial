<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<f:setLocale value="${language}" />
<f:setBundle basename="Texts" />

<t:layout>
    <jsp:attribute name="title">
        <f:message key="trip.new"/>
    </jsp:attribute>
    <jsp:body>
        <div class="row">
            <div id="content-with-menu" class="col-md-12">
                <h1><f:message key="trip.new"/></h1>
                <div class="col-md-2">
                    <ul class="nav nav-pills nav-stacked">
                        <li <c:if test="${activeMenu == 1}">class="active" </c:if>><a href="${pageContext.request.contextPath}/reservations"><f:message key="navigation.reservations"/></a></li>
                        <li <c:if test="${activeMenu == 2}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/trips"><f:message key="trips.title"/></a></li>
                        <li <c:if test="${activeMenu == 3}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/users"><f:message key="reservation.users"/></a></li>
                        <li <c:if test="${activeMenu == 4}">class="active" </c:if>><a href="${pageContext.request.contextPath}/admin/destinations"><f:message key="reservation.destinations"/></a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <form:form commandName="tripForm" method="post" action="${pageContext.request.contextPath}/admin/trip/create">
                        <form:errors path="*" cssClass="errorblock" element="p" />
                        <c:if test="${saveSuccess}">
                            <h3>Zájezd byl uložen</h3>
                        </c:if>
                        <table>
                            <tr><td><form:label path="name"><f:message key="admin.name"/></form:label></td><td><form:input path="name" type="text" /></td></tr>
                            <tr><td><form:label path="description"><f:message key="trip.description"/></form:label></td><td><form:textarea path="description" rows="5" cols="30" /></td></tr>
                            <tr><td><form:label path="startDate"><f:message key="trip.startDate"/></form:label></td><td><form:input type="text" path="startDate" class="datepicker" /></td>
                                <td><form:label path="endDate"><f:message key="trip.endDate"/></form:label></td><td><form:input type="text" path="endDate" class="datepicker" /></td></tr>
                            <tr><td><form:label path="destination"><f:message key="reservation.destination"/></form:label></td><td><form:select path="destination" items="${destinations}" /></td></tr>
                            <tr><td><form:label path="price"><f:message key="reservation.price"/></form:label></td><td><form:input path="price" type="number" /></td></tr>
                            <tr>
                                <td colspan="2">
                                    <form:button class="submitButton" type="submit"><f:message key="form.save" /></form:button>
                                </td>
                            </tr>
                        </table>
                
                
                    </form:form>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout>