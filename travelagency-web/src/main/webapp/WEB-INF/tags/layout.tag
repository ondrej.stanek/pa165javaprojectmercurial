<%@ tag pageEncoding="utf-8" dynamic-attributes="dynattrs" trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" fragment="true" required="true" %>
<%@ attribute name="head" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Texts" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title><jsp:invoke fragment="title"/></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/styles.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/jquery-ui.css" />
    
    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery.js"></script>
    <script src="${pageContext.servletContext.contextPath}/resources/js/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
</head>
<body>
<div id="container">
    <div id="banner">
        <div class="csschanger"><a href="${pageContext.servletContext.contextPath}/?language=cs"><f:message key="navigation.lang_czech"/></a>
                                | <a href="${pageContext.servletContext.contextPath}/?language=en"><f:message key="navigation.lang_english"/></a>

        </div>
        <div class="navcontainer">
          <ul>
            <li><a href="${pageContext.request.contextPath}/trips"><fmt:message key="navigation.trips"/></a></li>
            <c:if test="${pageContext.request.userPrincipal.name != null}">
            <li><a href="${pageContext.request.contextPath}/reservations"><fmt:message key="navigation.reservations"/></a></li>
            <sec:authorize ifAllGranted="ROLE_ADMIN">
            <li><a href="${pageContext.request.contextPath}/admin"><fmt:message key="navigation.administration"/></a></li>
            </sec:authorize>
            </c:if>
            <c:if test="${pageContext.request.userPrincipal.name == null}">
            <li><a href="${pageContext.request.contextPath}/registration"><fmt:message key="navigation.registration"/></a></li>
            </c:if>
          </ul>
        </div>
          <div class="right-nav">
                <c:if test="${pageContext.request.userPrincipal.name != null}">
                <div class="user-info">
                    Uživatel: ${pageContext.request.userPrincipal.name}
                </div>
                <a href="<c:url value="${pageContext.request.contextPath}/j_spring_security_logout" />" class="logout"><fmt:message key="login.logout"/></a>
                </c:if>
                <c:if test="${pageContext.request.userPrincipal.name == null}">
                    <a href="${pageContext.request.contextPath}/login" class="logout"><fmt:message key="login.login"/></a>
                </c:if>
          </div>
    </div>
    <div id="containerboth">
        <jsp:doBody />
    </div>
    <div id="footer">
        <div class="fotext">Copyright &copy; 2014. Design by PA165</div>
    </div>
    <script>
        $(function() {
            $(".datepicker").datepicker({dateFormat: "yy-mm-dd"});
        });
    </script>
</div>
</body>
</html>
