package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import travelagency.formentity.ChangePasswordForm;
import travelagency.formentity.RegisterUserForm;

/**
 * Form validator of password
 *
 * @author Boris Valo
 */
public class PasswordValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return ChangePasswordForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ChangePasswordForm form = (ChangePasswordForm) o;
        if (!form.getPassword().equals(form.getPasswordAgain())) {
            errors.rejectValue("password", "pass.notequal");
        }
    }
}
