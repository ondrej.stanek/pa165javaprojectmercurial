package travelagency.formvalidator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import travelagency.formentity.TripForm;

/**
 * Form validator of Trip
 *
 * @author Boris Valo
 */
public class TripValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return TripForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "trip.missingname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "trip.missingdesc");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "trip.missingstartdate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate", "trip.missingenddate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "trip.missingprice");
        
        TripForm form = (TripForm) o;
        
        if (form.getPrice() <= 0) {
            errors.rejectValue("price", "trip.missingprice");
        }
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate, endDate;
            startDate = df.parse(form.getStartDate());
            endDate = df.parse(form.getEndDate());
            if (startDate.compareTo(endDate) > 0) {
                errors.rejectValue("startDate", "trip.startdatetoobig");
            }
        } catch (Exception e) {
        }
    }

}
