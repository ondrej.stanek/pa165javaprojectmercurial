package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import travelagency.formentity.ReservationForm;

/**
 * Form validator of Reservation
 *
 * @author Boris Valo
 */
public class ReservationValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return ReservationForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ReservationForm form = (ReservationForm) o;
    }
}
