package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import travelagency.formentity.DestinationForm;

/**
 * Form validator of Destination
 *
 * @author Boris Valo
 */
public class DestinationValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return DestinationForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "dest.missingname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "dest.missingcountry");
        DestinationForm form = (DestinationForm) o;
    }

}
