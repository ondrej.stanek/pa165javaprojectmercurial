package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import travelagency.formentity.RegisterUserForm;

/**
 * Form validator of RegisterUser
 *
 * @author Jan Stralka
 */
public class RegisterUserValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return RegisterUserForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterUserForm form = (RegisterUserForm) o;
        if (!form.getPassword().equals(form.getPasswordAgain())) {
            errors.rejectValue("email", "pass.notequal");
        }
    }

}
