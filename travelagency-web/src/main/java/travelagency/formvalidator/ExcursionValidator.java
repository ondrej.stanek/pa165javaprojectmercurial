package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import travelagency.formentity.ExcursionForm;
import travelagency.formentity.TripForm;

/**
 * Form validator of Excursion
 *
 * @author Boris Valo
 */
public class ExcursionValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return ExcursionForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "exc.missingname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "exc.missingdesc");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "exc.missingstartdate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "endDate", "exc.missingenddate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "exc.missingprice");
        ExcursionForm form = (ExcursionForm) o;
    }
}
