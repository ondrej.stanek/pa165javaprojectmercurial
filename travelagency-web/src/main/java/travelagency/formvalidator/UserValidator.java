package travelagency.formvalidator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import travelagency.formentity.UserForm;

/**
 * Form validator of User
 *
 * @author Ondřej Staněk
 */
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class type) {
        return UserForm.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserForm form = (UserForm) o;
    }
}
