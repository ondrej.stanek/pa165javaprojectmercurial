package travelagency.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import travelagency.dto.CustomerDto;
import travelagency.service.CustomerService;

/**
 * Customer rest controller
 *
 * @author Ondřej Staněk
 */
@RestController
public class CustomerRestController {

    @Autowired
    CustomerService custService;

    /**
     * Getting of customer by id
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/customer/1
     * @param id customer id
     * @return customer
     */
    @RequestMapping("/rest/customer/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CustomerDto getById(@PathVariable("id") Long id) {
        CustomerDto c = custService.getById(id);
        return c;
    }

    /**
     * Getting of customer list
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/customers
     * @return customer list
     */
    @RequestMapping("/rest/customers")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<CustomerDto> getAll() {
        loginAsAdmin();
        return custService.getAll();
    }

    /**
     * Deleting of customer
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/customer/delete/1
     * @param id customer id
     */
    @RequestMapping("/rest/customer/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        loginAsAdmin();
        custService.delete(custService.getById(id));
    }

    /**
     * Adding of customer
     * For test you can use a command: curl.exe -X POST -H "Content-Type: application/json" --data "{\"name\":\"Jan\",\"email\":\"a@a.cz\",\"phone\":\"123456789\",\"password\":\"12345\",\"reservations\":[],\"born\":1418320147736,\"registered\":1418320168600}" http://localhost:8080/pa165/rest/customer/add
     * @param c customer
     * @return added customer
     */
    @RequestMapping("/rest/customer/add")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public CustomerDto add(@RequestBody CustomerDto c) {
        loginAsAdmin();
        CustomerDto cc = custService.add(c);
        return cc;
    }

    /**
     * Updating of customer
     * For test you can use a command: curl.exe -X POST -H "Content-Type: application/json" --data "{\"id\":1,\"name\":\"Jan\",\"email\":\"a@a.cz\",\"phone\":\"123456789\",\"password\":\"12345\",\"reservations\":[],\"born\":1418320147736,\"registered\":1418320168600}" http://localhost:8080/pa165/rest/customer/update
     * @param c customer for update
     * @return updated customer
     */
    @RequestMapping("/rest/customer/update")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public CustomerDto update(@RequestBody CustomerDto c) {
        loginAsAdmin();
        CustomerDto cc = custService.update(c);
        return cc;
    }

    private void loginAsAdmin() {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("rest", "rest", AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
