package travelagency.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import travelagency.dto.DestinationDto;
import travelagency.service.DestinationService;

/**
 * Destination rest controller
 *
 * @author Ondřej Staněk
 */
@RestController
public class DestinationRestController {

    @Autowired
    DestinationService destService;

    /**
     * Getting of destination by id
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/destination/1
     * @param id destination id
     * @return destination
     */
    @RequestMapping("/rest/destination/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public DestinationDto getById(@PathVariable("id") Long id) {
        DestinationDto d = destService.getById(id);
        return d;
    }

    /**
     * Getting of destination list
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/destinations
     * @return destination list
     */
    @RequestMapping("/rest/destinations")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<DestinationDto> getAll() {
        return destService.getAll();
    }

    /**
     * Deleting of destination
     * For test you can use a command: curl.exe http://localhost:8080/pa165/rest/destination/delete/1
     * @param id destination id
     */
    @RequestMapping("/rest/destination/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        loginAsAdmin();
        destService.delete(destService.getById(id));
    }

    /**
     * Adding of destination
     * For test you can use a command: curl.exe -X POST -H "Content-Type: application/json" --data "{\"name\":\"Brno\",\"country\":\"CR\"}" http://localhost:8080/pa165/rest/destination/add
     * @param d destination
     * @return added destination
     */
    @RequestMapping("/rest/destination/add")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public DestinationDto add(@RequestBody DestinationDto d) {
        loginAsAdmin();
        DestinationDto dd = destService.add(d);
        return dd;
    }

    /**
     * Updating of destination
     * For test you can use a command: curl.exe -X POST -H "Content-Type: application/json" --data "{\"id\":1,\"name\":\"Praha\",\"country\":\"CR\"}" http://localhost:8080/pa165/rest/destination/update
     * @param d destination for update
     * @return updated destination
     */
    @RequestMapping("/rest/destination/update")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public DestinationDto update(@RequestBody DestinationDto d) {
        loginAsAdmin();
        DestinationDto dd = destService.update(d);
        return dd;
    }
    
    private void loginAsAdmin() {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("rest", "rest", AuthorityUtils.createAuthorityList("ROLE_ADMIN"));
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
