package travelagency.webcontroller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.CustomerDto;
import travelagency.dto.DestinationDto;
import travelagency.dto.ExcursionDto;
import travelagency.dto.RoleDto;
import travelagency.dto.TripDto;
import travelagency.formentity.ChangePasswordForm;
import travelagency.formentity.DestinationForm;
import travelagency.formentity.ExcursionForm;
import travelagency.formentity.TripForm;
import travelagency.formentity.UserForm;
import travelagency.formvalidator.DestinationValidator;
import travelagency.formvalidator.ExcursionValidator;
import travelagency.formvalidator.PasswordValidator;
import travelagency.formvalidator.TripValidator;
import travelagency.formvalidator.UserValidator;
import travelagency.service.CustomerService;
import travelagency.service.DestinationService;
import travelagency.service.ExcursionService;
import travelagency.service.RoleService;
import travelagency.service.TripService;

/**
 * Admin controller
 * @author Jan Stralka
 */
@Controller
public class AdminController {
    @Autowired
    CustomerService customerService;
    
    @Autowired
    private TripService tripService;
    
    @Autowired
    private DestinationService destinationService;
    
    @Autowired
    private ExcursionService excursionService;
    
    @Autowired
    private RoleService roleService;
    
    @RequestMapping("/admin/destinations")
    public ModelAndView destinationsList() {

        ModelAndView model = new ModelAndView("admin/Destinations");
        List<DestinationDto> destinations = destinationService.getAll();
        model.addObject("destinations", destinations);
        model.addObject("title", "Seznam destinací");
        model.addObject("activeMenu", 4);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/create"}, method = {RequestMethod.GET})
    public ModelAndView destinationCreate() {

        ModelAndView model = new ModelAndView("admin/DestinationCreate");

        model.addObject("destinationForm", new DestinationForm());

        model.addObject("activeMenu", 4);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/create"}, method = {RequestMethod.POST})
    public ModelAndView destinationProcessCreate(@Valid DestinationForm destinationForm, BindingResult bindingResult) {

        ModelAndView model = new ModelAndView("admin/DestinationCreate");
        
        DestinationValidator validator = new DestinationValidator();
        validator.validate(destinationForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            model.addObject("destinationForm", destinationForm);
            
        }
        else
        {
            DestinationForm form = new DestinationForm();
            if(destinationForm.getId() != null){
                DestinationDto destination = destinationService.getById(destinationForm.getId());
                destination.setCountry(destinationForm.getCountry());
                destination.setName(destinationForm.getName());
                
                destinationService.update(destination);
                form = destinationForm;
                ModelAndView newModel = new ModelAndView("redirect:/admin/destination/edit/"+destinationForm.getId());
                newModel.addObject("saveSuccess", true);
                return newModel;
            }else{
                DestinationDto destination = DestinationDto.builder()
                    .name(destinationForm.getName())
                    .country(destinationForm.getCountry()).build();
                destinationService.add(destination);
            }
            model.addObject("destinationForm", form);
            model.addObject("saveSuccess", true);
        }
        
        model.addObject("activeMenu", 4);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationEdit(@PathVariable("id") Long id, @RequestParam(value = "saveSuccess", required=false) Boolean saveSuccess) {

        DestinationDto destination = destinationService.getById(id);
        
        ModelAndView model = new ModelAndView("admin/DestinationEdit");
        DestinationForm form = new DestinationForm();
        form.setCountry(destination.getCountry());
        form.setName(destination.getName());
        form.setId(destination.getId());
        model.addObject("destinationForm", form);

        if(saveSuccess != null && saveSuccess){
            model.addObject("saveSuccess", true);
        }
        
        model.addObject("activeMenu", 4);
        return model;
    }
    
    @RequestMapping(value = {"/admin/destination/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView destinationDelete(@PathVariable("id") Long id) {

        destinationService.delete(destinationService.getById(id));
        ModelAndView model = new ModelAndView("redirect:/admin/destinations");
        return model;
    }
    
    @RequestMapping("/admin/trips")
    public ModelAndView tripsListAdmin() {

        ModelAndView model = new ModelAndView("admin/Trips");
        List<TripDto> trips = tripService.getAll();
        model.addObject("trips", trips);
        model.addObject("title", "Seznam zájezdů");
        model.addObject("activeMenu", 2);
        return model;
    }

    @RequestMapping(value = {"/admin/trip/create"}, method = {RequestMethod.GET})
    public ModelAndView adminTripsCreate() {

        ModelAndView model = new ModelAndView("admin/TripCreate");
        
        model.addObject("tripForm", new TripForm());
        
        Map< Long, String > dests = new HashMap<>();  
        for(DestinationDto dest : destinationService.getAll()){
            dests.put(dest.getId(), dest.getName()+"("+dest.getCountry()+")");
        }
        model.addObject("destinations", dests);
        
        model.addObject("activeMenu", 2);
        return model;
    }
    
    @RequestMapping(value = {"/admin/trip/create"}, method = {RequestMethod.POST})
    public ModelAndView adminProcessTripsCreate(@Valid TripForm tripForm, BindingResult bindingResult) throws ParseException {

        ModelAndView model = new ModelAndView("admin/TripCreate");
        
        TripValidator validator = new TripValidator();
        validator.validate(tripForm, bindingResult);
        
        Map< Long, String > dests = new HashMap<>();  
        for(DestinationDto dest : destinationService.getAll()){
            dests.put(dest.getId(), dest.getName()+"("+dest.getCountry()+")");
        }
        model.addObject("destinations", dests);
        
        if (bindingResult.hasErrors()) {  
            model.addObject("tripForm", tripForm);
            model.addObject("errors", bindingResult.getAllErrors());
        }
        else
        {
            DestinationDto destination = destinationService.getById(tripForm.getDestination());
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
            Date startDate, endDate;
            TripForm form = new TripForm();
            
            if(tripForm.getId() != null){
                
                TripDto trip = tripService.getById(tripForm.getId());
                trip.setName(tripForm.getName());
                trip.setDescription(tripForm.getDescription());
                trip.setDestination(destination);
                trip.setPrice(tripForm.getPrice());
                
                startDate = df.parse(tripForm.getStartDate());
                endDate = df.parse(tripForm.getEndDate());
                
                trip.setStartDate(startDate);
                trip.setEndDate(endDate);
                
                tripService.update(trip);

                ModelAndView newModel = new ModelAndView("redirect:/admin/trip/edit/"+tripForm.getId());
                newModel.addObject("saveSuccess", true);
                return newModel;
            }else{
                
                
                startDate = df.parse(tripForm.getStartDate());
                endDate = df.parse(tripForm.getEndDate());
                TripDto trip = TripDto.builder().name(tripForm.getName())
                    .description(tripForm.getDescription())
                    .startDate(startDate).endDate(endDate)
                    .price(tripForm.getPrice()).destination(destination).build();
                
                tripService.add(trip);
            }
            model.addObject("tripForm", form);
            model.addObject("saveSuccess", true);
             
        }
        
        model.addObject("activeMenu", 2);
        return model;
    }
    
    @RequestMapping(value = {"/admin/trip/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView adminTripEdit(@PathVariable("id") Long id, @RequestParam(value = "saveSuccess", required=false) Boolean saveSuccess) {

        TripDto trip = tripService.getById(id);
        
        ModelAndView model = new ModelAndView("admin/TripEdit");
        TripForm form = new TripForm();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");         
        
        form.setDescription(trip.getDescription());
        form.setName(trip.getName());
        form.setStartDate(df.format(trip.getStartDate()));
        form.setEndDate(df.format(trip.getEndDate()));
        form.setDestination(trip.getDestination().getId());
        form.setId(trip.getId());
        form.setPrice(trip.getPrice());
        model.addObject("tripForm", form);

        Map< Long, String > dests = new HashMap<>();  
        for(DestinationDto dest : destinationService.getAll()){
            dests.put(dest.getId(), dest.getName()+"("+dest.getCountry()+")");
        }
        model.addObject("destinations", dests);
        
        model.addObject("excursions", excursionService.getTripExcursions(trip));
        
        ExcursionForm excForm = new ExcursionForm();
        excForm.setTrip(id);
        model.addObject("excursionForm", excForm);
        
        if(saveSuccess != null && saveSuccess){
            model.addObject("saveSuccess", true);
        }
        model.addObject("activeMenu", 2);
        return model;
    }

    @RequestMapping(value = {"/admin/excursion/create"}, method = {RequestMethod.POST})
    public ModelAndView adminProcessExcursionCreate(@Valid ExcursionForm excForm, BindingResult bindingResult) throws ParseException {

        ModelAndView model = new ModelAndView("admin/TripEdit");
        
        ExcursionValidator validator = new ExcursionValidator();
        validator.validate(excForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            model.addObject("excursionForm", excForm);
            model.addObject("errorsExc", bindingResult.getAllErrors());
        }
        else
        {
            TripDto trip = tripService.getById(excForm.getTrip());
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
            Date startDate, endDate;
            
            startDate = df.parse(excForm.getStartDate());
            endDate = df.parse(excForm.getEndDate());
            ExcursionDto exc = ExcursionDto.builder()
                .name(excForm.getName()).description(excForm.getDescription())
                .startDate(startDate).endDate(endDate)
                .price(excForm.getPrice()).trip(trip).build();

            excursionService.add(exc);
            
            model.addObject("saveSuccess", true);
            ModelAndView newModel = new ModelAndView("redirect:/admin/trip/edit/"+trip.getId());
            return newModel;
        }
        
        model.addObject("activeMenu", 2);
        return model;
    }
    
    @RequestMapping(value = {"/admin/trip/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView tripDelete(@PathVariable("id") Long id) {

        TripDto trip = tripService.getById(id);
        for(ExcursionDto exc : excursionService.getTripExcursions(trip)){
            excursionService.delete(exc);
        }
        tripService.delete(trip);
        ModelAndView model = new ModelAndView("redirect:/admin/trips");
        return model;
    }
    
    @RequestMapping(value = {"/admin/excursion/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView excursionDelete(@PathVariable("id") Long id) {

        ExcursionDto exc = excursionService.getById(id);
        Long tripId = exc.getTrip().getId();
        excursionService.delete(exc);
        ModelAndView model = new ModelAndView("redirect:/admin/trip/edit/"+tripId);
        return model;
    }
    
    @RequestMapping("/admin/users")
    public ModelAndView usersList() {

        ModelAndView model = new ModelAndView("admin/Users");
        model.addObject("activeMenu", 3);
        
        model.addObject("users", customerService.getAll());
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView userEdit(@PathVariable("id") Long id, @RequestParam(value = "saveSuccess", required=false) Boolean saveSuccess) {

        CustomerDto user = customerService.getById(id);
        
        ModelAndView model = new ModelAndView("admin/UserEdit");
        UserForm form = new UserForm();
        form.setEmail(user.getEmail());
        form.setId(user.getId());
        form.setName(user.getName());
        form.setPhone(user.getPhone());
        model.addObject("userForm", form);
        
        if(saveSuccess != null && saveSuccess){
            model.addObject("saveSuccess", true);
        }
        
        model.addObject("activeMenu", 3);
        return model;
    }
    
    @RequestMapping(value = {"/admin/users"}, method = {RequestMethod.POST})
    @SuppressWarnings("empty-statement")
    public ModelAndView userEditSave(@Valid UserForm userForm, BindingResult bindingResult) {

        ModelAndView model = new ModelAndView("admin/Users");
        
        UserValidator validator = new UserValidator();
        validator.validate(userForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            model.addObject("users", customerService.getAll());
            
        }
        else
        {
            UserForm form = new UserForm();
            
            if(userForm.getId() != null){
                CustomerDto user = customerService.getById(userForm.getId());
                user.setEmail(userForm.getEmail());
                user.setName(userForm.getName());
                user.setPhone(userForm.getPhone());
                
                customerService.update(user);
                form = userForm;
                ModelAndView newModel = new ModelAndView("redirect:/admin/users");
                newModel.addObject("saveSuccess", true);
                return newModel;
            }
            else {
                ;
            }
            
            model.addObject("users", customerService.getAll());
        }
        
        model.addObject("activeMenu", 3);
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/password/{id}"}, method = {RequestMethod.GET})
    public ModelAndView adminPasswordEdit(@PathVariable("id") Long id, @RequestParam(value = "saveSuccess", required=false) Boolean saveSuccess) {

        ModelAndView model = new ModelAndView("admin/UserPassword");
        ChangePasswordForm form = new ChangePasswordForm();
        form.setId(id);
        model.addObject("passForm", form);
        model.addObject("activeMenu", 2);
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/password/change"}, method = {RequestMethod.POST})
    public ModelAndView adminPasswordSave(@Valid ChangePasswordForm passForm, BindingResult bindingResult) {

        
        ModelAndView model = new ModelAndView("admin/UserPassword");
        PasswordValidator validator = new PasswordValidator();
        validator.validate(passForm, bindingResult);
        
        if (bindingResult.hasErrors()) {
            model.addObject("passForm", passForm);
            model.addObject("errorsExc", bindingResult.getAllErrors());
        }
        else
        {
            CustomerDto cust = customerService.getById(passForm.getId());
            
            cust.setPassword(passForm.getPassword());
            customerService.update(cust);
            
            model.addObject("saveSuccess", true);
            ModelAndView newModel = new ModelAndView("redirect:/admin/users");
            return newModel;
        }
        model.addObject("activeMenu", 2);
        return model;
    }
    
    @RequestMapping(value = {"/admin/user/delete/{id}"}, method = {RequestMethod.GET})
    public String userDelete(@PathVariable("id") Long id) {

        customerService.delete(customerService.getById(id));
        return "redirect:/admin/users";
    }
    
    @RequestMapping(value = {"/admin/user/setadmin/{id}"}, method = {RequestMethod.GET})
    public String userSetAdmin(@PathVariable("id") Long id) {

        CustomerDto user = customerService.getById(id);
        List<RoleDto> roles = roleService.getByRole("ROLE_ADMIN");
        if (roles.size() == 1) {
            user.setRole(roles.get(0));
            customerService.update(user);
        }
        return "redirect:/admin/users";
    }
    
    @RequestMapping(value = {"/admin/user/unsetadmin/{id}"}, method = {RequestMethod.GET})
    public String userUnsetAdmin(@PathVariable("id") Long id) {
        CustomerDto user = customerService.getById(id);
        List<RoleDto> roles = roleService.getByRole("ROLE_USER");
        if (roles.size() == 1) {
            user.setRole(roles.get(0));
            customerService.update(user);
        }
        
        return "redirect:/admin/users";
    }
}
