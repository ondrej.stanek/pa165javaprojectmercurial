package travelagency.webcontroller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.RoleDto;
import travelagency.service.RoleService;

@Controller
public class IndexController {

    public static String ROLE_USER = "ROLE_USER";
    public static String ROLE_ADMIN = "ROLE_ADMIN";
    
    @Autowired
    private RoleService roleService;
    /**
     * Main method for main page view
     */
    @RequestMapping("/")
    public ModelAndView index() {
        initializeRoles();
        ModelAndView model = new ModelAndView("index");
        return model;
    }
    
    private void initializeRoles() {
        List<RoleDto> roles =  roleService.getAll();
        if(roles.isEmpty()){
            RoleDto role1 = RoleDto.builder().role(ROLE_ADMIN).build();
            roleService.add(role1);
            RoleDto role2 = RoleDto.builder().role(ROLE_USER).build();
            roleService.add(role2);
        }
    }
}
