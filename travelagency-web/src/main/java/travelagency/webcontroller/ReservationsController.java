package travelagency.webcontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.CustomerDto;
import travelagency.dto.ExcursionDto;
import travelagency.dto.ReservationDto;
import travelagency.dto.TripDto;
import travelagency.formentity.ReservationForm;
import travelagency.formvalidator.ReservationValidator;
import travelagency.service.CustomerService;
import travelagency.service.DestinationService;
import travelagency.service.ExcursionService;
import travelagency.service.ReservationService;
import travelagency.service.TripService;

/**
 * Reservation controller
 * @author Boris Valo
 */
@Controller
public class ReservationsController {

    @Autowired
    private TripService tripService;

    @Autowired
    private ExcursionService excursionService;

    @Autowired
    CustomerService customerService;

    @Autowired
    ReservationService reservationService;

    @Autowired
    private DestinationService destinationService;

    @RequestMapping(value = {"/reservations", "/admin"})
    public ModelAndView myReservations(HttpServletRequest request) {
        Map<String, String[]> parameters = request.getParameterMap();

        ModelAndView model = new ModelAndView("Reservations");
        String findStr = null;
        if (parameters.containsKey("find")) {
            findStr = parameters.get("find")[0];
            if (findStr.trim().equals(""))
            {
                findStr = null;
            }
        }
        model.addObject("findStr", findStr);
        if (findStr != null)
        {
            model.addObject("reservations", reservationService.findByString(findStr));
        } else {
            model.addObject("reservations", reservationService.getAll());
        }

        model.addObject("activeMenu", 1);
        return model;
    }

    @RequestMapping(value = {"/reservation/delete/{id}", "/admin/reservation/delete/{id}"}, method = {RequestMethod.GET})
    public ModelAndView resertvationDelete(@PathVariable("id") Long id) {

        reservationService.delete(reservationService.getById(id));
        ModelAndView model = new ModelAndView("redirect:/reservations");
        return model;
    }

    @RequestMapping(value = {"/reservation/new/{id}"}, method = {RequestMethod.GET})
    public ModelAndView makeReservation(@PathVariable("id") Long id) {

        ModelAndView model = new ModelAndView("Trip");
        ReservationForm form = new ReservationForm();
        form.setTrip(id);
        model.addObject("reserveForm", form);

        TripDto trip = tripService.getById(id);
        model.addObject("trip", trip);

        Map< Long, String> excs = new HashMap<>();
        for (ExcursionDto exc : excursionService.getTripExcursions(trip)) {
            excs.put(exc.getId(), exc.getName());
        }
        model.addObject("excursions", excs);

        Map< Long, String> custs = new HashMap<>();
        for (CustomerDto cust : customerService.getAll()) {
            custs.put(cust.getId(), cust.getName());
        }
        model.addObject("users", custs);

        model.addObject("menuType", "dest");
        model.addObject("destinations", destinationService.getAll());

        return model;
    }

    @RequestMapping(value = {"/reservation/done"}, method = {RequestMethod.POST})
    public ModelAndView reservationDone(@Valid ReservationForm resForm, BindingResult bindingResult) {

        ModelAndView model = new ModelAndView("Trip");
        ReservationValidator validator = new ReservationValidator();
        validator.validate(resForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addObject("reserveForm", resForm);
            model.addObject("errorsExc", bindingResult.getAllErrors());
        } else {
            CustomerDto cust = customerService.getById(resForm.getCustomer());
            TripDto trip = tripService.getById(resForm.getTrip());
            ReservationDto reservation = null;
            ModelAndView newModel = null;

            if (resForm.getReservationId() != null && resForm.getReservationId() > 0) {
                reservation = reservationService.getById(resForm.getReservationId());
                reservation.setComment(resForm.getComment());
                reservation.setCustomer(cust);
                reservation.setTrip(trip);
                reservation.getExcursions().clear();
                if (resForm.getExcursions() != null) {
                    for (Long id : resForm.getExcursions()) {
                        reservation.getExcursions().add(excursionService.getById(id));
                    }
                }

                reservationService.update(reservation);
                newModel = new ModelAndView("redirect:/reservation/edit/" + reservation.getId());
            } else {
                reservation = ReservationDto.builder().customer(cust).trip(trip)
                    .comment(resForm.getComment()).build();

                if (resForm.getExcursions() != null) {
                    for (Long id : resForm.getExcursions()) {
                        reservation.getExcursions().add(excursionService.getById(id));
                    }
                }

                reservationService.add(reservation);
                newModel = new ModelAndView("redirect:/reservations/");
            }
            newModel.addObject("saveSuccess", true);

            return newModel;
        }
        model.addObject("activeMenu", 1);
        return model;
    }

    @RequestMapping(value = {"/reservation/edit/{id}"}, method = {RequestMethod.GET})
    public ModelAndView editReservation(@PathVariable("id") Long id, @RequestParam(value = "saveSuccess", required = false) Boolean saveSuccess) {

        ModelAndView model = new ModelAndView("Trip");
        ReservationForm form = new ReservationForm();
        form.setReservationId(id);

        ReservationDto reservation = reservationService.getById(id);
        TripDto trip = reservation.getTrip();
        form.setTrip(trip.getId());
        form.setComment(reservation.getComment());
        form.setCustomer(reservation.getCustomer().getId());

        List<Long> defExcs = new ArrayList();
        for (ExcursionDto defEx : reservation.getExcursions()) {
            defExcs.add(defEx.getId());
        }
        form.setExcursions(defExcs);

        model.addObject("reserveForm", form);
        model.addObject("trip", trip);

        Map< Long, String> excs = new HashMap<>();
        for (ExcursionDto exc : excursionService.getTripExcursions(trip)) {
            excs.put(exc.getId(), exc.getName());
        }
        model.addObject("excursions", excs);

        Map< Long, String> custs = new HashMap<>();
        for (CustomerDto cust : customerService.getAll()) {
            custs.put(cust.getId(), cust.getName());
        }
        model.addObject("users", custs);

        model.addObject("menuType", "admin");
        model.addObject("activeMenu", 1);

        if (saveSuccess != null && saveSuccess) {
            model.addObject("saveSuccess", true);
        }

        return model;
    }

}
