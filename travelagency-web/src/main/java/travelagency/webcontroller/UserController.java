package travelagency.webcontroller;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.CustomerDto;
import travelagency.dto.RoleDto;
import travelagency.formentity.RegisterUserForm;
import travelagency.formvalidator.RegisterUserValidator;
import travelagency.service.CustomerService;
import travelagency.service.RoleService;

/**
 * User controller
 * @author Boris Valo
 */
@Controller
public class UserController {
    public static String ROLE_USER = "ROLE_USER";
    public static String ROLE_ADMIN = "ROLE_ADMIN";
    
    @Autowired
    CustomerService customerService;
    
    @Autowired
    private RoleService roleService;
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        initializeRoles();
        ModelAndView model = new ModelAndView("Registration");
        model.addObject("registerUserForm", new RegisterUserForm());

        return model;
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registrationPost(@Valid RegisterUserForm registerUser, BindingResult bindingResult) {
        initializeRoles();
        ModelAndView model = new ModelAndView("Registration");
        RegisterUserValidator registerUserValidator = new RegisterUserValidator();
        registerUserValidator.validate(registerUser, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addObject("registerUserForm", registerUser);
            
        }
        else
        {
            model.addObject("registerUserForm", new RegisterUserForm());
            
            CustomerDto customer = CustomerDto.builder()
                .id(null).name(registerUser.getName()).email(registerUser.getEmail())
                .phone(registerUser.getPhone()).password(registerUser.getPassword())
                .born(new Date()).registered(new Date())
                .role(RoleDto.builder().role(ROLE_USER).build()).build();
            List<RoleDto> roleList = roleService.getByRole(ROLE_USER);
            if (!roleList.isEmpty()) {
                RoleDto role = roleList.get(0);
                customer.setRole(role);
                customerService.add(customer);
                model.addObject("registerSuccess", true);
            }
        }
        
        return model;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginIndex(Principal principal) {
        initializeRoles();
        ModelAndView model = new ModelAndView("Login");
        if (principal != null) {
            String email = principal.getName();
            List<CustomerDto> list = customerService.getByEmail(email);
            if (!list.isEmpty()) {
                String name = list.get(0).getName();
                model.addObject("username", name);
            }
        }
        return model;
    }
    
    @RequestMapping(value = "/login/error", method = RequestMethod.GET)
    public ModelAndView loginInvalidAction() {
        initializeRoles();
        ModelAndView modelAndView = new ModelAndView("Login");
        modelAndView.addObject("error", true);

        return modelAndView;
    }
    
    // pro stránku 403 přístup zamítnut
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accesssDenied(Principal principal) {
        initializeRoles();
        ModelAndView model = new ModelAndView();

        if (principal != null) {
                String email = principal.getName();
                List<CustomerDto> list = customerService.getByEmail(email);
                if (!list.isEmpty()) {
                    String name = list.get(0).getName();
                    model.addObject("msg", "Uživatel " + name + " nemá přístup na tuto stránku.");
                }
        }
            else {
                model.addObject("msg", "Nemáte oprávnění k přístupu na tuto stránku.");
        }

            model.setViewName("403");
        return model;
    }
    
    private void initializeRoles() {
        List<RoleDto> roles =  roleService.getAll();
        if(roles.isEmpty()){
            RoleDto role1 = RoleDto.builder().role(ROLE_ADMIN).build();
            roleService.add(role1);
            RoleDto role2 = RoleDto.builder().role(ROLE_USER).build();
            roleService.add(role2);
        }
    }
}
