package travelagency.webcontroller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import travelagency.dto.DestinationDto;
import travelagency.dto.RoleDto;
import travelagency.dto.TripDto;
import travelagency.service.DestinationService;
import travelagency.service.RoleService;
import travelagency.service.TripService;

/**
 * Trip controller
 * @author Boris Valo
 */
@Controller
public class TripsController {
    
    public static String ROLE_USER = "ROLE_USER";
    public static String ROLE_ADMIN = "ROLE_ADMIN";

    @Autowired
    private TripService tripService;
    
    @Autowired
    private DestinationService destinationService;
    
    @Autowired
    private RoleService roleService;

    @RequestMapping(value = {"/trips"}, method = {RequestMethod.GET})
    public ModelAndView showTrips() {
        initializeRoles();
        ModelAndView model = new ModelAndView("Trips");
        
        List<TripDto> trips = null;
        trips = tripService.getAll();
        
        model.addObject("trips", trips);
        model.addObject("destinations", destinationService.getAll());
        return model;
    }

    @RequestMapping(value = {"/trips/{country}"}, method = {RequestMethod.GET})
    public ModelAndView showTripsByCountry(@PathVariable(value = "country") Long country) {
        initializeRoles();
        ModelAndView model = new ModelAndView("Trips");
        
        DestinationDto dest = destinationService.getById(country);
        List<TripDto> trips = tripService.getByDestination(dest);
        model.addObject("active", country);
        
        model.addObject("trips", trips);
        model.addObject("destinations", destinationService.getAll());
        return model;
    }
    
    private void initializeRoles() {
        List<RoleDto> roles =  roleService.getAll();
        if(roles.isEmpty()){
            RoleDto role1 = RoleDto.builder().role(ROLE_ADMIN).build();
            roleService.add(role1);
            RoleDto role2 = RoleDto.builder().role(ROLE_USER).build();
            roleService.add(role2);
        }
    }
}
