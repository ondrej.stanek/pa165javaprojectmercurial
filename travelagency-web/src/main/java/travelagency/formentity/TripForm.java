package travelagency.formentity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Form for trip
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TripForm {

    @EqualsAndHashCode.Include private Long id;

    @Size(min = 2, max = 50)
    private String name;

    @Size(min = 2, max = 300)
    private String description;

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private Double price;

    @NotNull
    private Long destination;
}
