package travelagency.formentity;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Form for reservation
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ReservationForm {

    @EqualsAndHashCode.Include private Long reservationId;

    private Long customer;

    private Long trip;

    private String comment;

    private List<Long> excursions = new ArrayList<>();
}
