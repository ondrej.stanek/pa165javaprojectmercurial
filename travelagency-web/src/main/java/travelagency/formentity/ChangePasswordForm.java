package travelagency.formentity;

import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Form for changing password
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ChangePasswordForm {

    @EqualsAndHashCode.Include private Long id;

    @Size(min = 5, max = 50, message = "Heslo by má mít 5 až 50 znaků")
    private String password;

    @Size(min = 5, max = 50)
    private String passwordAgain;
}
