package travelagency.formentity;

import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Form for destination creating
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DestinationForm {

    @EqualsAndHashCode.Include private Long id;

    @Size(min = 2, max = 50)
    private String name;

    @Size(min = 2, max = 50)
    private String country;
}
