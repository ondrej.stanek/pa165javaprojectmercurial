package travelagency.formentity;

import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;

/**
 * Form for user registration
 *
 * @author Jan Stralka, Ondřej Staněk
 * @see http://spring.io/guides/gs/validating-form-input/
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class RegisterUserForm {

    @Size(min = 2, max = 50, message = "Vyplňte jméno")
    private String name;

    @Email
    @EqualsAndHashCode.Include
    private String email;

    @Size(min = 5, max = 50, message = "Heslo by má mít 5 až 50 znaků")
    private String password;

    @Size(min = 5, max = 50)
    private String passwordAgain;

    @Pattern(regexp = "\\+?[0-9]{9,14}", message = "Zadejte telefonní číslo ve formátu (+420)123456789.")
    private String phone;
}
