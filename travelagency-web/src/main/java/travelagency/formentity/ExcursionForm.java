package travelagency.formentity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Form for creating excursion
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ExcursionForm {

    @EqualsAndHashCode.Include private Long id;

    @Size(min = 2, max = 50)
    private String name;

    @Size(min = 2, max = 300)
    private String description;

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private double price;

    @NotNull
    private Long trip;
}
