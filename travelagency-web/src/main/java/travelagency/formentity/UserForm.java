package travelagency.formentity;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Email;

/**
 * Form for user.
 *
 * @author Boris, Ondřej Staněk
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserForm {

    @EqualsAndHashCode.Include private Long id;

    @Size(min = 2, max = 50, message = "Vyplňte jméno")
    private String name;

    @Email
    private String email;

    @Pattern(regexp = "\\+?[0-9]{9,14}", message = "Zadejte telefonní číslo ve formátu (+420)123456789.")
    private String phone;
}
