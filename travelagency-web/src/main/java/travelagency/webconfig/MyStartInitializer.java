package travelagency.webconfig;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.jstl.core.Config;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Start initializer
 *
 * @author Jan Stralka, Ondřej Staněk
 */
public class MyStartInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {      
        AnnotationConfigWebApplicationContext ctx = createConfiguredSpringBeansContext();
        registerSpringMvcDispatcherServlet(servletContext, ctx);
        registerFilterEncodingSetting(servletContext);
        registerLocalizationBundle(servletContext);
    }
    
    /**
     * Creating Spring beans context configured in MySpringMvcConfig.class
     * @return context
     */
    private AnnotationConfigWebApplicationContext createConfiguredSpringBeansContext() {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        ctx.register(MySpringMvcConfig.class);
        return ctx;
    }

    /**
     * Registering Spring MVC main Dispatcher servlet
     * @param servletContext servlet context
     * @param ctx beans context
     */
    private void registerSpringMvcDispatcherServlet(ServletContext servletContext,
                                                    AnnotationConfigWebApplicationContext ctx) {
        ServletRegistration.Dynamic disp = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
        disp.setLoadOnStartup(1);
        disp.addMapping("/");
    }
    
    /**
     * Registering filter utf-8 encoding setting on all requests
     * @param servletContext servlet context
     */
    private void registerFilterEncodingSetting(ServletContext servletContext) {
        FilterRegistration.Dynamic encoding = servletContext.addFilter("encoding", CharacterEncodingFilter.class);
        encoding.setInitParameter("encoding", "utf-8");
        encoding.addMappingForUrlPatterns(null, false, "/*");
    }
    
    /**
     * Register localization bundle also for JSTL fmt: tags which are not behind DispatcherServlet
     * @param servletContext servlet context
     */
    private void registerLocalizationBundle(ServletContext servletContext) {
        servletContext.setInitParameter(Config.FMT_LOCALIZATION_CONTEXT, "Texts");
    }
}
