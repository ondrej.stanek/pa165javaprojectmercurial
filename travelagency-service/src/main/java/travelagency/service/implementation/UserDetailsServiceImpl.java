package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import travelagency.dto.CustomerDto;
import travelagency.dto.RoleDto;
import travelagency.entity.Customer;
import travelagency.manager.CustomerManager;

/**
 *
 * @author Ondřej Staněk
 */
@Service("userDetailsService")
@Transactional(readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
    @Getter @Setter
    private CustomerManager manager;
    
    /**
     * Load user by user name
     * 
     * @param email email
     * @return user detail for authentication
     * @throws UsernameNotFoundException when username doesn't exist.
     */
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        
        List<Customer> list = manager.getByEmail(email);
        
        if (list.isEmpty())
            return null;
        
        Customer cust = list.get(0);

        if (cust == null){
            throw new UsernameNotFoundException("Username not found.");
        }

        CustomerDto custDto = CustomerServiceImpl.EntityToDto(cust);
        RoleDto roleDto = RoleServiceImpl.EntityToDto(cust.getRole());
        custDto.setRole(roleDto);

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new User(
            custDto.getEmail(),
            custDto.getPassword(),
            enabled,
            accountNonExpired,
            credentialsNonExpired,
            accountNonLocked,
            getAuthorities(custDto.getRole())
        );
    }

    /**
     * Get authorities
     * 
     * @param role
     * @return 
     */
    private Collection<? extends GrantedAuthority> getAuthorities(RoleDto role)
    {
        return getGrantedAuthorities(getRoles(role));
    }

    /**
     * Create a list and add the role there.
     * 
     * @param role role
     * @return list with the role added
     */
    private List<String> getRoles(RoleDto role)
    {
        List<String> roles = new ArrayList<>();
        roles.add(role.getRole());
        return roles;
    }

    /**
     * Get granted authorities
     *
     * @param roles roles
     * @return list of granted authorities
     */
    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles)
    {
        List<GrantedAuthority> authorities = new ArrayList<>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }

        return authorities;
    }
}
