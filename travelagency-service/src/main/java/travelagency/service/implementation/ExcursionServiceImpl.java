package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import travelagency.dto.ExcursionDto;
import travelagency.dto.ReservationDto;
import travelagency.dto.TripDto;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;
import travelagency.manager.ExcursionManager;
import travelagency.service.ExcursionService;

/**
 * Service implementation of Excursion
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Service
@Slf4j
public class ExcursionServiceImpl implements ExcursionService {

    @Autowired
    @Getter @Setter
    private ExcursionManager manager;

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public ExcursionDto add(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            ee = manager.create(ee);
            if (ee != null) {
                return EntityToDto(ee);
            } else {
                return null;
            }
        } catch (DataAccessException ex) {
            log.error("Error: Excursion add:", ex);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public ExcursionDto update(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            ee = manager.update(ee);
            if (ee != null) {
                return EntityToDto(ee);
            } else {
                return null;
            }
        } catch (DataAccessException ex) {
            log.error("Error: Excursion update:", ex);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(ExcursionDto e) {
        Excursion ee = DtoToEntity(e);
        try {
            manager.delete(ee);
        } catch (DataAccessException ex) {
            log.error("Error: Excursion delete:", ex);
        }
    }

    @Override
    @Transactional
    public ExcursionDto getById(Long id) {
        try {
            Excursion e = manager.findById(id);
            return EntityToDto(e);
        } catch (DataAccessException ex) {
            log.error("Error: Excursion getById:", ex);
            return null;
        }
    }

    @Override
    @Transactional
    public List<ExcursionDto> getAll() {
        List<Excursion> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException ex) {
            log.error("Error: Excursion getAll:", ex);
            return null;
        }
        List<ExcursionDto> l2 = new ArrayList<>();
        ExcursionDto ee;
        for (Excursion e : l1) {
            ee = EntityToDto(e);
            l2.add(ee);
        }
        return l2;
    }

    @Override
    @Transactional
    public List<ExcursionDto> getTripExcursions(TripDto t) {
        List<Excursion> l1;
        try {
            Trip t1 = TripServiceImpl.DtoToEntity(t);
            l1 = manager.getTripExcursions(t1);
        } catch (DataAccessException ex) {
            log.error("Error: Excursion getTripExcursions:", ex);
            return null;
        }
        List<ExcursionDto> l2 = new ArrayList<>();
        ExcursionDto ee;
        for (Excursion e : l1) {
            ee = EntityToDto(e);
            l2.add(ee);
        }
        return l2;
    }

    /**
     * Excursion entity to dto
     *
     * @param exc excursion
     * @return null or excursion dto
     */
    public static ExcursionDto EntityToDto(Excursion exc) {
        ExcursionDto dto = new ExcursionDto();

        dto.setId(exc.getId());
        dto.setName(exc.getName());
        dto.setDescription(exc.getDescription());
        dto.setPrice(exc.getPrice());
        dto.setStartDate(exc.getStartDate());
        dto.setEndDate(exc.getEndDate());
        dto.setTrip(TripServiceImpl.EntityToDto(exc.getTrip()));

        return dto;
    }

    /**
     * Dto to excursion entity
     *
     * @param exc excursion
     * @return null or entity
     */
    public static Excursion DtoToEntity(ExcursionDto exc) {
        if (exc == null) {
            return null;
        }
        Excursion entity = new Excursion();

        entity.setId(exc.getId());
        entity.setName(exc.getName());
        entity.setDescription(exc.getDescription());
        entity.setPrice(exc.getPrice());
        entity.setStartDate(exc.getStartDate());
        entity.setEndDate(exc.getEndDate());
        entity.setTrip(TripServiceImpl.DtoToEntity(exc.getTrip()));
        Set<Reservation> reservations = new HashSet<>();
        for (ReservationDto r : exc.getReservations()) {
            reservations.add(ReservationServiceImpl.DtoToEntity(r));
        }
        entity.setReservations(reservations);

        return entity;
    }
}
