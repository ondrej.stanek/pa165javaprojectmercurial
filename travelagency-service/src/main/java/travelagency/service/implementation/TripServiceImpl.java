package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import travelagency.dto.DestinationDto;
import travelagency.dto.TripDto;
import travelagency.entity.Destination;
import travelagency.entity.Trip;
import travelagency.manager.TripManager;
import travelagency.service.TripService;

/**
 * Service implementation of Trip
 *
 * @author Ondřej Staněk, Boris Valo
 */
@Service
@Slf4j
public class TripServiceImpl implements TripService {

    @Autowired
    @Getter @Setter
    private TripManager manager;

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public TripDto add(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            tt = manager.create(tt);
            return EntityToDto(tt);
        } catch (DataAccessException e) {
            log.error("Error: Trip add:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public TripDto update(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            tt = manager.update(tt);
            return EntityToDto(tt);
        } catch (DataAccessException e) {
            log.error("Error: Trip update:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(TripDto t) {
        Trip tt = DtoToEntity(t);
        try {
            manager.delete(tt);
        } catch (DataAccessException e) {
            log.error("Error: Trip delete:", e);
        }
    }

    @Override
    @Transactional
    public TripDto getById(Long id) {
        try {
            Trip t = manager.findById(id);
            return EntityToDto(t);
        } catch (DataAccessException e) {
            log.error("Error: Trip getById:", e);
            return null;
        }
    }

    @Override
    @Transactional
    public List<TripDto> getAll() {
        List<Trip> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException e) {
            log.error("Error: Trip getAll:", e);
            return null;
        }
        List<TripDto> l2 = new ArrayList<>();
        TripDto tt;
        for (Trip t : l1) {
            tt = EntityToDto(t);
            l2.add(tt);
        }
        return l2;
    }

    @Override
    @Transactional
    public List<TripDto> getByDestination(DestinationDto d) {
        List<Trip> l1;
        try {
            Destination d1 = DestinationServiceImpl.DtoToEntity(d);
            l1 = manager.getByDestination(d1);
        } catch (DataAccessException e) {
            log.error("Error: Trip getByDestination:", e);
            return null;
        }
        List<TripDto> l2 = new ArrayList<>();
        TripDto tt;
        for (Trip t : l1) {
            tt = EntityToDto(t);
            l2.add(tt);
        }
        return l2;
    }

    /**
     * Trip entity to dto
     *
     * @param trip trip
     * @return null or trip dto
     */
    public static TripDto EntityToDto(Trip trip) {
        if (trip == null) {
            return null;
        }
        TripDto dto = new TripDto();

        dto.setId(trip.getId());
        dto.setName(trip.getName());
        dto.setDescription(trip.getDescription());
        dto.setPrice(trip.getPrice());
        dto.setStartDate(trip.getStartDate());
        dto.setEndDate(trip.getEndDate());
        dto.setDestination(DestinationServiceImpl.EntityToDto(trip.getDestination()));

        return dto;
    }

    /**
     * Dto to trip entity
     *
     * @param trip trip
     * @return null or entity
     */
    public static Trip DtoToEntity(TripDto trip) {
        if (trip == null) {
            return null;
        }
        Trip entity = new Trip();

        entity.setId(trip.getId());
        entity.setName(trip.getName());
        entity.setDescription(trip.getDescription());
        entity.setPrice(trip.getPrice());
        entity.setStartDate(trip.getStartDate());
        entity.setEndDate(trip.getEndDate());
        entity.setDestination(DestinationServiceImpl.DtoToEntity(trip.getDestination()));

        return entity;
    }
}
