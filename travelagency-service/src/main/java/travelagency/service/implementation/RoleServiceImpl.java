package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import travelagency.dto.RoleDto;
import travelagency.entity.Role;
import travelagency.manager.RoleManager;
import travelagency.service.RoleService;

/**
 *
 * @author Ondřej Staněk
 */
@Service
@Slf4j
public class RoleServiceImpl implements RoleService {
    
    @Autowired
    @Getter @Setter
    private RoleManager manager;

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public RoleDto add(RoleDto role) {
        Role r = DtoToEntity(role);
        try {
            r = manager.create(r);
            return EntityToDto(r);
        } catch (DataAccessException e) {
            log.error("Error: Role add:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public RoleDto update(RoleDto role) {
        Role r = DtoToEntity(role);
        try {
            r = manager.update(r);
            return EntityToDto(r);
        } catch (DataAccessException e) {
            log.error("Error: Role update:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(RoleDto role) {
        Role r = DtoToEntity(role);
        try {
            manager.delete(r);
        } catch (DataAccessException e) {
            log.error("Error: Role delete:", e);
        }
    }

    @Override
    @Transactional
    public List<RoleDto> getAll() {
        List<Role> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException e) {
            log.error("Error: Role getAll:", e);
            return null;
        }
        List<RoleDto> l2 = new ArrayList<>();
        RoleDto rr;
        for (Role r : l1) {
            rr = EntityToDto(r);
            l2.add(rr);
        }
        return l2;
    }
    
    @Override
    @Transactional
    public List<RoleDto> getByRole(String role) {
        List<Role> l1;
        try {
            l1 = manager.getByRole(role);
        } catch (DataAccessException e) {
            log.error("Error: Role getByRole:", e);
            return null;
        }
        List<RoleDto> l2 = new ArrayList<>();
        RoleDto tt;
        for (Role t : l1) {
            tt = EntityToDto(t);
            l2.add(tt);
        }
        return l2;
    }
    
    /**
     * Role entity to dto
     *
     * @param role
     * @return null or role dto
     */
    public static RoleDto EntityToDto(Role role) {
        if (role == null) {
            return null;
        }

        RoleDto dto = new RoleDto();

        dto.setId(role.getId());
        dto.setRole(role.getRole());

        return dto;
    }

    /**
     * Dto to role entity
     *
     * @param role
     * @return null or entity
     */
    public static Role DtoToEntity(RoleDto role) {
        if (role == null) {
            return null;
        }
        Role entity = new Role();

        entity.setId(role.getId());
        entity.setRole(role.getRole());

        return entity;
    }
}
