package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import travelagency.dto.ExcursionDto;
import travelagency.dto.ReservationDto;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.manager.ReservationManager;
import travelagency.service.ReservationService;

/**
 * Service implementation of Reservation
 *
 * @author Ondřej Staněk, Boris Valo
 */
@Service
@Slf4j
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    @Getter @Setter
    private ReservationManager manager;

    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ReservationDto add(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            rr = manager.create(rr);
            return EntityToDto(rr);
        } catch (DataAccessException e) {
            log.error("Error: Reservation add:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ReservationDto update(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            rr = manager.update(rr);
            return EntityToDto(rr);
        } catch (DataAccessException e) {
            log.error("Error: Reservation update:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public void delete(ReservationDto r) {
        Reservation rr = DtoToEntity(r);
        try {
            manager.delete(rr);
        } catch (DataAccessException e) {
            log.error("Error: Reservation delete:", e);
        }
    }

    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ReservationDto getById(Long id) {
        try {
            Reservation r = manager.findById(id);
            return EntityToDto(r);
        } catch (DataAccessException e) {
            log.error("Error: Reservation getById:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<ReservationDto> getAll() {
        List<Reservation> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException e) {
            log.error("Error: Reservation getAll:", e);
            return null;
        }
        List<ReservationDto> l2 = new ArrayList<>();
        ReservationDto rr;
        for (Reservation r : l1) {
            rr = EntityToDto(r);
            l2.add(rr);
        }
        return l2;
    }

    @Override
    public List<ReservationDto> findByString(String find) {
        List<Reservation> matchedReservations;
        try {
            matchedReservations = manager.findByString(find);
        } catch (DataAccessException e) {
            log.error("Error: Reservation getByCustomer:", e);
            return null;
        }
        List<ReservationDto> reservationDtoList = new ArrayList<>();
        ReservationDto reservationDto;
        for (Reservation r : matchedReservations) {
            reservationDto = EntityToDto(r);
            reservationDtoList.add(reservationDto);
        }
        return reservationDtoList;
    }

    /**
     * Reservation entity to dto
     *
     * @param res reservation
     * @return null or reservation dto
     */
    public static ReservationDto EntityToDto(Reservation res) {
        if (res == null) {
            return null;
        }
        ReservationDto dto = new ReservationDto();

        dto.setId(res.getId());
        dto.setComment(res.getComment());
        dto.setCustomer(CustomerServiceImpl.EntityToDto(res.getCustomer()));
        dto.setTrip(TripServiceImpl.EntityToDto(res.getTrip()));
        List<ExcursionDto> newExc = new ArrayList<>();
        ExcursionDto ee;
        for (Excursion e : res.getExcursions()) {
            ee = ExcursionServiceImpl.EntityToDto(e);
            newExc.add(ee);
        }
        dto.setExcursions(newExc);

        return dto;
    }

    /**
     * Dto to reservation entity
     *
     * @param res reservation
     * @return null or entity
     */
    public static Reservation DtoToEntity(ReservationDto res) {
        if (res == null) {
            return null;
        }
        Reservation entity = new Reservation();

        entity.setId(res.getId());
        entity.setComment(res.getComment());
        entity.setCustomer(CustomerServiceImpl.DtoToEntity(res.getCustomer()));
        entity.setTrip(TripServiceImpl.DtoToEntity(res.getTrip()));
        Set<Excursion> newExc = new HashSet<>();
        Excursion ee;
        for (ExcursionDto e : res.getExcursions()) {
            ee = ExcursionServiceImpl.DtoToEntity(e);
            newExc.add(ee);
        }
        entity.setExcursions(newExc);

        return entity;
    }
}
