package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import travelagency.dto.CustomerDto;
import travelagency.entity.Customer;
import travelagency.manager.CustomerManager;
import travelagency.service.CustomerService;

/**
 * Service implementation of Customer
 *
 * @author Ondřej Staněk, Boris Valo
 */
@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    @Getter @Setter
    private CustomerManager manager;

    @Override
    @Transactional
    public CustomerDto add(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            c = manager.create(c);
            return EntityToDto(c);
        } catch (DataAccessException e) {
            log.error("Error: Customer add:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public CustomerDto update(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            c = manager.update(c);
            return EntityToDto(c);
        } catch (DataAccessException e) {
            log.error("Error: Customer update:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(CustomerDto customer) {
        Customer c = DtoToEntity(customer);
        try {
            manager.delete(c);
        } catch (DataAccessException e) {
            log.error("Error: Customer delete:", e);
        }
    }

    @Override
    @Transactional
    public CustomerDto getById(Long id) {
        try {
            Customer c = manager.findById(id);
            return EntityToDto(c);
        } catch (DataAccessException e) {
            log.error("Error: Customer getById:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public List<CustomerDto> getAll() {
        List<Customer> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException e) {
            log.error("Error: Customer getAll:", e);
            return null;
        }
        List<CustomerDto> l2 = new ArrayList<>();
        CustomerDto cc;
        for (Customer c : l1) {
            cc = EntityToDto(c);
            l2.add(cc);
        }
        return l2;
    }
    
    @Override
    @Transactional
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public List<CustomerDto> getByEmail(String email) {
        List<Customer> l1;
        try {
            l1 = manager.getByEmail(email);
        } catch (DataAccessException e) {
            log.error("Error: Customer getByEmail:", e);
            return null;
        }
        List<CustomerDto> l2 = new ArrayList<>();
        CustomerDto tt;
        for (Customer t : l1) {
            tt = EntityToDto(t);
            l2.add(tt);
        }
        return l2;
    }

    /**
     * Customer entity to dto
     *
     * @param customer
     * @return null or customer dto
     */
    public static CustomerDto EntityToDto(Customer customer) {
        if (customer == null) {
            return null;
        }

        CustomerDto dto = new CustomerDto();

        dto.setId(customer.getId());
        dto.setName(customer.getName());
        dto.setEmail(customer.getEmail());
        dto.setBorn(customer.getBorn());
        dto.setPassword(customer.getPassword());
        dto.setPhone(customer.getPhone());
        dto.setRegistered(customer.getRegistered());
        dto.setRole(RoleServiceImpl.EntityToDto(customer.getRole()));

        return dto;
    }

    /**
     * Dto to customer entity
     *
     * @param customer
     * @return null or entity
     */
    public static Customer DtoToEntity(CustomerDto customer) {
        if (customer == null) {
            return null;
        }
        Customer entity = new Customer();

        entity.setId(customer.getId());
        entity.setName(customer.getName());
        entity.setEmail(customer.getEmail());
        entity.setBorn(customer.getBorn());
        entity.setPassword(customer.getPassword());
        entity.setPhone(customer.getPhone());
        entity.setRegistered(customer.getRegistered());
        entity.setRole(RoleServiceImpl.DtoToEntity(customer.getRole()));

        return entity;
    }
}
