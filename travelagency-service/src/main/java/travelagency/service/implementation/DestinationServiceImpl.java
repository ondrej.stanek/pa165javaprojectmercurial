package travelagency.service.implementation;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import travelagency.dto.DestinationDto;
import travelagency.entity.Destination;
import travelagency.manager.DestinationManager;
import travelagency.service.DestinationService;

/**
 * Service implementation of Destination
 *
 * @author Ondřej Staněk, Boris Valo
 */
@Service
@Slf4j
public class DestinationServiceImpl implements DestinationService {

    @Autowired
    @Getter @Setter
    private DestinationManager manager;

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public DestinationDto add(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            dd = manager.create(dd);
            return EntityToDto(dd);
        } catch (DataAccessException e) {
            log.error("Error: Destination add:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public DestinationDto update(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            dd = manager.update(dd);
            return EntityToDto(dd);
        } catch (DataAccessException e) {
            log.error("Error: Destination update:", e);
            return null;
        }
    }

    @Override
    @Transactional
    @Secured("ROLE_ADMIN")
    public void delete(DestinationDto d) {
        Destination dd = DtoToEntity(d);
        try {
            manager.delete(dd);
        } catch (DataAccessException e) {
            log.error("Error: Destination delete:", e);
        }
    }

    @Override
    @Transactional
    public DestinationDto getById(Long id) {
        try {
            Destination d = manager.findById(id);
            return EntityToDto(d);
        } catch (DataAccessException e) {
            log.error("Error: Destination getById:", e);
            return null;
        }
    }

    @Override
    @Transactional
    public List<DestinationDto> getAll() {
        List<Destination> l1;
        try {
            l1 = manager.listAll();
        } catch (DataAccessException e) {
            log.error("Error: Destination getAll:", e);
            return null;
        }
        List<DestinationDto> l2 = new ArrayList<>();
        DestinationDto dd;
        for (Destination d : l1) {
            dd = EntityToDto(d);
            l2.add(dd);
        }
        return l2;
    }

    /**
     * Destination entity to dto
     *
     * @param dest destination
     * @return null or destination dto
     */
    public static DestinationDto EntityToDto(Destination dest) {
        if (dest == null) {
            return null;
        }
        DestinationDto dto = new DestinationDto();

        dto.setId(dest.getId());
        dto.setName(dest.getName());
        dto.setCountry(dest.getCountry());
        return dto;
    }

    /**
     * Dto to destination entity
     *
     * @param dest destination
     * @return null or entity
     */
    public static Destination DtoToEntity(DestinationDto dest) {
        if (dest == null) {
            return null;
        }
        Destination entity = new Destination();

        entity.setId(dest.getId());
        entity.setName(dest.getName());
        entity.setCountry(dest.getCountry());
        return entity;
    }
}
