/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import travelagency.dto.RoleDto;
import travelagency.entity.Role;
import travelagency.manager.implementation.RoleManagerImpl;
import travelagency.service.implementation.RoleServiceImpl;

/**
 *
 * @author Jan Stralka <jsem@janstralka.cz>
 */
@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTest {
    @Autowired
    @InjectMocks
    RoleServiceImpl service;

    @Mock
    RoleManagerImpl managerImplMock;
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        if (managerImplMock == null) {
            throw new RuntimeException("managerImplMock = null error!");
        }
        if (service == null) {
            throw new RuntimeException("service = null error!");
        }
    }
    
    @After
    public void tearDown() {
    }
    
    private static void assertEntity(Role role, RoleDto roleDto) {
        assertEquals(roleDto.getId(), role.getId());
        assertEquals(roleDto.getRole(), role.getRole());
    }
    
    private static void assertEntityCaptor(RoleDto roleDto, ArgumentCaptor<Role> captor) {
        assertEquals(roleDto.getId(), captor.getValue().getId());
        assertEquals(roleDto.getRole(), captor.getValue().getRole());
    }
    
    @Test
    public void testAdd() {
        RoleDto itemDto = RoleDto.builder().role("ROLE_ADMIN").build();
        ArgumentCaptor<Role> captor = ArgumentCaptor.forClass(Role.class);
        service.add(itemDto);
        Mockito.verify(managerImplMock).create(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }
    
    @Test
    public void testDelete() {
        RoleDto itemDto = RoleDto.builder().role("ROLE_ADMIN").build();
        ArgumentCaptor<Role> captor = ArgumentCaptor.forClass(Role.class);
        service.delete(itemDto);
        Mockito.verify(managerImplMock).delete(captor.capture());
        assertEntityCaptor(itemDto, captor);
    }
    
    @Test
    public void testGetAll() {
        List<Role> allExpected = new ArrayList<>();
        Role role1 = new Role("ROLE_ADMIN");
        Role role2 = new Role("ROLE_USER");
        role1.setId(1L);
        role2.setId(2L);
        allExpected.add(role1);
        allExpected.add(role2);
        
        Mockito.stub(managerImplMock.listAll())
                .toReturn(allExpected);
        List<RoleDto> allActual = service.getAll();
        Mockito.verify(managerImplMock).listAll();
        assertEquals(allActual.size(), 2);
        assertEntity(allExpected.get(0), allActual.get(0));
        assertEntity(allExpected.get(1), allActual.get(1));
    }
}
