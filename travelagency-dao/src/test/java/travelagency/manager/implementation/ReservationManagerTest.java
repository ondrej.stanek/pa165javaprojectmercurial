/*
 * This is a test class of Reservation entity. It tests CRUD operations of reservations and all other possible methods.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Destination;
import travelagency.entity.Trip;
import travelagency.entity.Reservation;
import travelagency.entity.Customer;
import travelagency.entity.Excursion;
import travelagency.entity.Role;

/**
 *
 * @author Boris
 */
public class ReservationManagerTest {

    private static final double DELTA = 1e-15;

    private ReservationManagerImpl manager;
    private TripManagerImpl tripManager;
    private DestinationManagerImpl destManager;
    private CustomerManagerImpl custManager;
    private RoleManagerImpl roleManager;

    private EntityManagerFactory emFactory;
    private EntityManager em;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new ReservationManagerImpl();
        tripManager = new TripManagerImpl();
        destManager = new DestinationManagerImpl();
        custManager = new CustomerManagerImpl();
        roleManager = new RoleManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
        em = emFactory.createEntityManager();
        manager.setEm(em);
        tripManager.setEm(em);
        destManager.setEm(em);
        custManager.setEm(em);
        roleManager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();

    }

    /**
     * Create a valid reservation.
     *
     * @result Reservation will be persisted without any errors, and
     * Reservation.getId() will no longer be null
     */
    @Test
    public void createReservation() {
        System.out.println("Metoda createReservation()");

        em.getTransaction().begin();
        Reservation res = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(res);
        em.getTransaction().commit();

        Long id = res.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Reservation result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(res, result);
        assertDeepEquals(res, result);

        cleanup();
    }

    /**
     * Delete an existing reservation.
     *
     * @result reservation will be deleted
     */
    @Test
    public void deleteReservation() {
        System.out.println("Metoda deleteReservation()");

        em.getTransaction().begin();
        Reservation r1 = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(r1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(r2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(r1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(r1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(r2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Update an existing reservation.
     *
     * @result reservation will be updated
     */
    @Test
    public void updateReservation() {
        System.out.println("Metoda updateReservation()");

        em.getTransaction().begin();
        Reservation res = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(res);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();

        Long id = res.getId();

        Destination dest = new Destination("Praha", "CR");

        em.getTransaction().begin();
        destManager.create(dest);
        em.getTransaction().commit();

        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        
        Role role = new Role("ROLE_USER");       
        em.getTransaction().begin();
        roleManager.create(role);
        em.getTransaction().commit();
        
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1), role);

        em.getTransaction().begin();
        res = manager.findById(id);
        em.getTransaction().commit();

        res.setComment("Cirkus");

        em.getTransaction().begin();
        manager.update(res);
        em.getTransaction().commit();

        assertEquals("Cirkus", res.getComment());
        assertDeepEquals(trip, res.getTrip());
        assertDeepEquals(cust, res.getCustomer());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(r2, manager.findById(r2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Get all reservations.
     *
     * @result list of all reservations ordered by id.
     */
    @Test
    public void getAllReservations() {
        System.out.println("Metoda getAllReservations()");

        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        em.getTransaction().begin();
        Reservation r1 = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        Reservation r2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();

        List<Reservation> expected = Arrays.asList(r1, r2);

        em.getTransaction().begin();
        List<Reservation> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

    private void assertDeepEquals(List<Reservation> expectedList, List<Reservation> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Reservation expected = expectedList.get(i);
            Reservation actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Reservation expected, Reservation actual) {
        assertEquals(expected.getComment(), actual.getComment());
        assertDeepEquals(expected.getCustomer(), actual.getCustomer());
        assertDeepEquals(expected.getTrip(), actual.getTrip());
    }

    private void assertDeepEquals(Trip expected, Trip actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getStartDate(), actual.getStartDate());
        assertEquals(expected.getEndDate(), actual.getEndDate());
        assertEquals(expected.getPrice(), actual.getPrice(), DELTA);
        assertDeepEquals(expected.getDestination(), actual.getDestination());
    }

    private void assertDeepEquals(Destination expected, Destination actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }

    private void assertDeepEquals(Customer expected, Customer actual) {
        assertEquals(expected.getBorn(), actual.getBorn());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getRegistered(), actual.getRegistered());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertDeepEquals(expected.getRole(), actual.getRole());
    }
    
    private void assertDeepEquals(Role expected, Role actual) {
        assertEquals(expected.getRole(), actual.getRole());
    }

    private void cleanup() {
    }

    private static Comparator<Reservation> idComparator = new Comparator<Reservation>() {

        @Override
        public int compare(Reservation o1, Reservation o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };

    private Reservation createNew1() {

        Destination dest = new Destination("Praha", "CR");
        destManager.create(dest);
        Trip trip = new Trip("Vejlet", "Nevím kam", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);
        Role role = new Role("ROLE_USER");
        roleManager.create(role);
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1), role);
        custManager.create(cust);

        Reservation res = new Reservation(cust, trip, "Moc se tesime", new HashSet<Excursion>());

        return res;
    }

    private Reservation createNew2() {
        Destination dest = new Destination("Kolin", "GE");
        destManager.create(dest);
        Trip trip = new Trip("Konici", "Jsou krasni", new Date(114, 1, 11), new Date(114, 1, 11), 22, dest);
        tripManager.create(trip);
        Role role = new Role("ROLE_USER");
        roleManager.create(role);
        Customer cust = new Customer("Standa", "asdas@voprs.cz", "123456789", "asdasda", new Date(90, 10, 10), new Date(114, 1, 1), role);
        custManager.create(cust);

        Reservation res = new Reservation(cust, trip, "Jeste sem tam nebyl", new HashSet<Excursion>());

        return res;
    }

}
