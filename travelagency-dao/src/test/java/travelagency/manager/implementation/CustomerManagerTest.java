/*
 * This is a test class of Customer entity. It tests CRUD operations of customers and all other possible methods.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Customer;
import travelagency.entity.Role;

/**
 *
 * @author Boris
 */
public class CustomerManagerTest {

    private CustomerManagerImpl manager;
    
    private RoleManagerImpl roleManager;

    private EntityManagerFactory emFactory;

    private EntityManager em;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new CustomerManagerImpl();
        roleManager = new RoleManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
        em = emFactory.createEntityManager();
        manager.setEm(em);
        roleManager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    /**
     * Create a valid customer.
     *
     * @result Customer will be persisted without any errors, and
     * Customer.getId() will no longer be null
     */
    @Test
    public void createCustomer() {
        System.out.println("Metoda createCustomer()");
        
        em.getTransaction().begin();
        Role role = createRole();
        em.getTransaction().commit();
        Customer cust = new Customer("Petr Vopršálek", "petr@voprs.cz", "123456789", "asdf", new Date(90, 10, 10), new Date(114, 1, 1), role);
        
        em.getTransaction().begin();
        manager.create(cust);
        em.getTransaction().commit();

        Long id = cust.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Customer result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(cust, result);
        assertDeepEquals(cust, result);

        cleanup();
    }

    /**
     * Delete an existing customer.
     *
     * @result Customer will be deleted
     */
    @Test
    public void deleteCustomer() {
        System.out.println("Metoda deleteCustomer()");
        
        em.getTransaction().begin();
        Role role = createRole();
        em.getTransaction().commit();
        Customer c1 = new Customer("Petr", "a@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1), role);
        
        em.getTransaction().begin();
        Role role2 = createRole();
        em.getTransaction().commit();
        Customer c2 = new Customer("pavel", "c@d.cz", "123456789", "asdfefgh", new Date(92, 4, 10), new Date(114, 1, 1), role2);

        em.getTransaction().begin();
        manager.create(c1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(c1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(c2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(c1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(c1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(c2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Update an existing customer.
     *
     * @result Customer will be updated
     */
    @Test
    public void updateCustomer() {
        System.out.println("Metoda updateCustomer()");

        em.getTransaction().begin();
        Role role1 = createRole();
        em.getTransaction().commit();
        Customer cust = new Customer("Boris", "adff@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1), role1);
        
        em.getTransaction().begin();
        Role role2 = createRole();
        em.getTransaction().commit();
        Customer c2 = new Customer("Marek", "afff@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1), role2);

        em.getTransaction().begin();
        manager.create(cust);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();

        Role role3 = new Role("ROLE_USER");

        em.getTransaction().begin();
        roleManager.create(role3);
        em.getTransaction().commit();
        
        Long id = cust.getId();

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setName("Honza");

        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("adff@b.cz", cust.getEmail());
        assertEquals(new Date(91, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setEmail("honzik@ja.cz");

        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(91, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setBorn(new Date(90, 1, 10));

        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 1, 1), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setRegistered(new Date(114, 2, 10));
        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("123456789", cust.getPhone());
        assertEquals("asdf", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setPhone("987654321");

        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("987654321", cust.getPhone());
        assertEquals("asdf", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        em.getTransaction().begin();
        cust = manager.findById(id);
        em.getTransaction().commit();

        cust.setPassword("1233");

        em.getTransaction().begin();
        manager.update(cust);
        em.getTransaction().commit();

        assertEquals("Honza", cust.getName());
        assertEquals("honzik@ja.cz", cust.getEmail());
        assertEquals(new Date(90, 1, 10), cust.getBorn());
        assertEquals(new Date(114, 2, 10), cust.getRegistered());
        assertEquals("987654321", cust.getPhone());
        assertEquals("1233", cust.getPassword());
        assertDeepEquals(role3, cust.getRole());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(c2, manager.findById(c2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Get all customers.
     *
     * @result List of all customers ordered by id.
     */
    @Test
    public void getAllCustomers() {
        System.out.println("Metoda getAllCustomers()");

        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        em.getTransaction().begin();
        Role role1 = createRole();
        em.getTransaction().commit();
        Customer c1 = new Customer("dfxgds", "ssa@b.cz", "123456789", "asdf", new Date(91, 1, 10), new Date(114, 1, 1), role1);
        
        em.getTransaction().begin();
        Role role2 = createRole();
        em.getTransaction().commit();
        Customer c2 = new Customer("dfgdfgd", "c@ddd.cz", "123456789", "asdfefgh", new Date(92, 4, 10), new Date(114, 1, 1), role2);

        em.getTransaction().begin();
        manager.create(c1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(c2);
        em.getTransaction().commit();

        List<Customer> expected = Arrays.asList(c1, c2);

        em.getTransaction().begin();
        List<Customer> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

    private void assertDeepEquals(List<Customer> expectedList, List<Customer> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Customer expected = expectedList.get(i);
            Customer actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Customer expected, Customer actual) {
        assertTrue(actual.equals(expected));
        assertEquals(expected.getBorn(), actual.getBorn());
        assertEquals(expected.getEmail(), actual.getEmail());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getRegistered(), actual.getRegistered());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getPhone(), actual.getPhone());
        assertDeepEquals(expected.getRole(), actual.getRole());
    }
    
    private void assertDeepEquals(Role expected, Role actual) {
        assertEquals(expected.getRole(), actual.getRole());
    }

    private void cleanup() {
    }

    private static Comparator<Customer> idComparator = new Comparator<Customer>() {

        @Override
        public int compare(Customer o1, Customer o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };
    
    private Role createRole() {
        Role role = new Role("ROLE_USER");
        role = roleManager.create(role);
        return role;
    }
}
