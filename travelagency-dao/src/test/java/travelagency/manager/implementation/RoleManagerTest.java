/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelagency.manager.implementation;

import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import travelagency.entity.Role;

/**
 *
 * @author Jan Stralka <jsem@janstralka.cz>
 */
public class RoleManagerTest {
    private RoleManagerImpl manager;
    
    private EntityManagerFactory emFactory;
    private EntityManager em;
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new RoleManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
        em = emFactory.createEntityManager();
        manager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    /**
     * Create a valid trip.
     *
     * @result Trip will be persisted without any errors, and Trip.getId() will
     * no longer be null
     */
    @Test
    public void createRole() {
        System.out.println("Metoda createTrip()");

        em.getTransaction().begin();
        Role role = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(role);
        em.getTransaction().commit();

        String roleName = role.getRole();
        assertNotNull(roleName);
        
        em.getTransaction().begin();
        Role result = getByRole(roleName);
        em.getTransaction().commit();
        
        assertEquals(role, result);
        assertDeepEquals(role, result);
    }
    
    @Test
    public void deleteRole() {
        em.getTransaction().begin();
        Role role = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(role);
        em.getTransaction().commit();

        String roleName = role.getRole();
        assertNotNull(roleName);
        
        em.getTransaction().begin();
        assertNotNull(getByRole(roleName));
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        manager.delete(role);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        assertNull(getByRole(roleName));
        em.getTransaction().commit();
        
        
    }
    
    @Test
    public void listAllRoles() {
        em.getTransaction().begin();
        Role r1 = createNew1();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r1);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        Role r2 = createNew2();
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(r2);
        em.getTransaction().commit();
        
        em.getTransaction().begin();
        List<Role> roles = manager.listAll();
        em.getTransaction().commit();
        
        assertEquals(roles.size(), 2);
    }
            
            

    private Role createNew1() {
        Role role = new Role("ROLE_ADMIN");
        return role;
    }
    
    private Role createNew2() {
        Role role = new Role("ROLE_USER");
        return role;
    }
    
    private Role getByRole(String roleName)
    {
        List<Role> result = manager.getByRole(roleName);
        if (result.size() != 1)
        {
            return null;
        }
        return result.get(0);
    }
    
    private static void assertDeepEquals(Role role1, Role role2) {
        assertEquals(role1.getId(), role2.getId());
        assertEquals(role1.getRole(), role2.getRole());
    }
}
