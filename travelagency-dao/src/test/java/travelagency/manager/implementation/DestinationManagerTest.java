/*
 * This is a test class of Destination entity. It tests CRUD operations of destinations and all other possible methods.
 */
package travelagency.manager.implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelagency.entity.Destination;

/**
 *
 * @author Boris
 */
public class DestinationManagerTest {

    private DestinationManagerImpl manager;

    private EntityManagerFactory emFactory;

    private EntityManager em;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        manager = new DestinationManagerImpl();
        emFactory = Persistence.createEntityManagerFactory("putest");
        em = emFactory.createEntityManager();
        manager.setEm(em);
    }

    @After
    public void tearDown() {
        em.close();
        emFactory.close();
    }

    /**
     * Create a valid destination.
     *
     * @result Destination will be persisted without any errors, and
     * Destination.getId() will no longer be null
     */
    @Test
    public void createDestination() {
        System.out.println("Metoda createDestination()");
        Destination dest = new Destination("Home", "Česká republika");

        em.getTransaction().begin();
        manager.create(dest);
        em.getTransaction().commit();

        Long id = dest.getId();
        assertNotNull(id);

        em.getTransaction().begin();
        Destination result = manager.findById(id);
        em.getTransaction().commit();

        assertEquals(dest, result);
        assertDeepEquals(dest, result);

        cleanup();
    }

    /**
     * Delete an existing destination.
     *
     * @result destination will be deleted
     */
    @Test
    public void deleteDestination() {
        System.out.println("Metoda deleteDestination()");
        Destination d1 = new Destination("Home", "čr");
        Destination d2 = new Destination("Kolín", "ge");

        em.getTransaction().begin();
        manager.create(d1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(d2);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(d1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(d2.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.delete(d1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNull(manager.findById(d1.getId()));
        em.getTransaction().commit();

        em.getTransaction().begin();
        assertNotNull(manager.findById(d2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Update an existing destination.
     *
     * @result destination will be updated
     */
    @Test
    public void updateDestination() {
        System.out.println("Metoda updateDestination()");

        Destination dest = new Destination("Home", "čr");
        Destination d2 = new Destination("Kolín", "ge");

        em.getTransaction().begin();
        manager.create(dest);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(d2);
        em.getTransaction().commit();

        Long id = dest.getId();

        em.getTransaction().begin();
        dest = manager.findById(id);
        em.getTransaction().commit();

        dest.setName("Praha");

        em.getTransaction().begin();
        manager.update(dest);
        em.getTransaction().commit();

        assertEquals("Praha", dest.getName());
        assertEquals("čr", dest.getCountry());

        em.getTransaction().begin();
        dest = manager.findById(id);
        em.getTransaction().commit();

        dest.setCountry("sk");

        em.getTransaction().begin();
        manager.update(dest);
        em.getTransaction().commit();

        assertEquals("Praha", dest.getName());
        assertEquals("sk", dest.getCountry());

        // Check if updates didn't affected other records
        em.getTransaction().begin();
        assertDeepEquals(d2, manager.findById(d2.getId()));
        em.getTransaction().commit();

        cleanup();
    }

    /**
     * Get all destinations.
     *
     * @result list of all destinations ordered by id.
     */
    @Test
    public void getAllDestinations() {
        System.out.println("Metoda getAllDestinations()");

        em.getTransaction().begin();
        assertTrue(manager.listAll().isEmpty());
        em.getTransaction().commit();

        Destination d1 = new Destination("Home", "čr");
        Destination d2 = new Destination("Kolín", "ge");

        em.getTransaction().begin();
        manager.create(d1);
        em.getTransaction().commit();

        em.getTransaction().begin();
        manager.create(d2);
        em.getTransaction().commit();

        List<Destination> expected = Arrays.asList(d1, d2);

        em.getTransaction().begin();
        List<Destination> actual = manager.listAll();
        em.getTransaction().commit();

        Collections.sort(actual, idComparator);
        Collections.sort(expected, idComparator);

        assertEquals(expected, actual);
        assertDeepEquals(expected, actual);

        cleanup();
    }

    private void assertDeepEquals(List<Destination> expectedList, List<Destination> actualList) {
        for (int i = 0; i < expectedList.size(); i++) {
            Destination expected = expectedList.get(i);
            Destination actual = actualList.get(i);
            assertDeepEquals(expected, actual);
        }
    }

    private void assertDeepEquals(Destination expected, Destination actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getCountry(), actual.getCountry());
    }

    private void cleanup() {

    }

    private static Comparator<Destination> idComparator = new Comparator<Destination>() {

        @Override
        public int compare(Destination o1, Destination o2) {
            return Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId()));
        }
    };

}
