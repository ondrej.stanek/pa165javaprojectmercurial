package travelagency.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * This is an entity of Excursion.
 *
 * @author Boris Valo, Ondřej Staněk
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Excursion implements Serializable {

    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    protected Long id;

    private String name;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    private double price;

    @ManyToOne
    private Trip trip;

    @ManyToMany(mappedBy = "excursions", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private Set<Reservation> reservations = new HashSet<>();

    /**
     * Constructor
     *
     * @param name name
     * @param description description
     * @param start start date
     * @param end end date
     * @param price price
     * @param trip trip which contains this excursion
     */
    public Excursion(String name, String description, Date start, Date end, double price, Trip trip) {
        this.name = name;
        this.description = description;
        this.startDate = start;
        this.endDate = end;
        this.price = price;
        this.trip = trip;
    }

    /**
     * Adding the reservation.
     *
     * @param r the reservation which is added
     */
    public void addReservation(Reservation r) {
        if (!reservations.contains(r)) {
            reservations.add(r);
        }
    }

    /**
     * Removing the reservation.
     *
     * @param r the reservation which is removed
     */
    public void deleteReservation(Reservation r) {
        if (reservations.contains(r)) {
            reservations.remove(r);
        }
    }
}
