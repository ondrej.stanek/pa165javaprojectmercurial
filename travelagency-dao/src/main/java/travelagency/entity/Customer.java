package travelagency.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * This is an entity of Customer.
 *
 * @author Jan Stralka, Ondřej Staněk
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Customer implements Serializable {

    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    protected Long id;

    private String name;

    private String email;

    private String phone;

    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    private Date born;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registered;
    
    @ManyToOne
    private Role role;

    /**
     * Constructor
     *
     * @param name name
     * @param email email
     * @param phone phone
     * @param password password
     * @param born born date
     * @param registered registration date
     * @param role role
     */
    public Customer(String name, String email, String phone, String password, Date born, Date registered, Role role) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.born = born;
        this.registered = registered;
        this.role = role;
    }
}
