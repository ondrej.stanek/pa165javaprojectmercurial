package travelagency.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * This is an entity of Trip.
 *
 * @author Ondřej Staněk
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Trip implements Serializable {

    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private String name;

    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    private double price;

    @ManyToOne
    private Destination destination;

    /**
     * Constructor
     *
     * @param name name
     * @param description description
     * @param start date of start
     * @param end date of end
     * @param price price
     * @param destination destination
     */
    public Trip(String name, String description, Date start, Date end, double price, Destination destination) {
        this.name = name;
        this.description = description;
        this.startDate = start;
        this.endDate = end;
        this.price = price;
        this.destination = destination;
    }
}
