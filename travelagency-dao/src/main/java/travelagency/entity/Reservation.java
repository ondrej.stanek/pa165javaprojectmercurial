package travelagency.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * This is an entity of Reservation.
 *
 * @author Jan Stralka, Ondřej Staněk
 */
@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Reservation implements Serializable {

    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    protected Long id;

    @ManyToOne(optional = false)
    private Customer customer;

    @ManyToOne(optional = false)
    private Trip trip;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "reservation_excursion",
            joinColumns = {
                @JoinColumn(name = "RESERVATION_ID", nullable = false)},
            inverseJoinColumns = {
                @JoinColumn(name = "EXCURSION_ID", nullable = false)})
    private Set<Excursion> excursions = new HashSet<>();

    private String comment;

    /**
     * Constructor
     *
     * @param customer customer who made reservation
     * @param trip trip which was chosen
     * @param comment comment
     * @param excursions excursions which were chosen
     */
    public Reservation(Customer customer, Trip trip, String comment, Set<Excursion> excursions) {
        this.customer = customer;
        this.trip = trip;
        this.comment = comment;
        this.excursions = excursions;
    }

    /**
     * Adding the excursion among the other reserved excursions.
     *
     * @param e the excursion which is added
     */
    public void addExcursion(Excursion e) {
        if (!excursions.contains(e)) {
            excursions.add(e);
        }
    }

    /**
     * Removing the excursion from the other reserved excursions.
     *
     * @param e the excursion which is removed
     */
    public void deleteExcursion(Excursion e) {
        if (excursions.contains(e)) {
            excursions.remove(e);
        }
    }
}
