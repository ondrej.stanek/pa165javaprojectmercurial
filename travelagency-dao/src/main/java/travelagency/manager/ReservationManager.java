package travelagency.manager;

import java.util.List;
import travelagency.entity.Customer;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;

/**
 * The interface of reservation manager
 *
 * @author Ondřej Staněk
 */
public interface ReservationManager {

    /**
     * Creating of the reservation. If it has some excursions, they are merged
     *
     * @param r reservation, if its id is not null, it sets it to null
     * @return excursion created
     */
    Reservation create(Reservation r);

    /**
     * Deleting the reservation
     *
     * @param r reservation which is going to be deleted
     */
    void delete(Reservation r);

    /**
     * Updating the reservation
     *
     * @param r reservation which is going to be updated
     * @return reservation updated
     */
    Reservation update(Reservation r);

    /**
     * Make the list of all reservations in the system
     *
     * @return list of the reservations
     */
    List<Reservation> listAll();

    /**
     * Searching the reservation by id
     *
     * @param id the id of reservation, which is searched
     * @return reservation with this id
     */
    Reservation findById(Long id);

    /**
     * Make the list of reservations connected to trip t
     *
     * @param t the trip which is reserved
     * @return the list of all reservations of the trip t
     */
    List<Reservation> getByTrip(Trip t);

    /**
     * Make the list of reservations connected to customer c
     *
     * @param c the customer who make reservations
     * @return the list of all reservations of the customer c
     */
    List<Reservation> getByCustomer(Customer c);

    /**
     * Searching the reservations which contains the string find in the name
     *
     * @param find the sequence of characters which are searched
     * @return the list of all reservations which contains string find
     */
    List<Reservation> findByString(String find);
}
