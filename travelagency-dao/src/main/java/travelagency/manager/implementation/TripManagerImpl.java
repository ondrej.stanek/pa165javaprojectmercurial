/**
 * The implementation of trip manager
 */
package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import travelagency.entity.Destination;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;
import travelagency.manager.TripManager;

/**
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class TripManagerImpl implements TripManager {

    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Trip create(Trip t) {
        if (t.getId() != null) {
            t.setId(null);
        }
        em.persist(t);
        return t;
    }

    @Override
    public void delete(Trip t) {
        Trip t1 = em.find(Trip.class, t.getId());
        ExcursionManagerImpl exManager = new ExcursionManagerImpl();
        exManager.setEm(em);
        List<Excursion> excursions = exManager.getTripExcursions(t1);
        for (Excursion e : excursions) {
            exManager.delete(e);
        }
        ReservationManagerImpl resManager = new ReservationManagerImpl();
        resManager.setEm(em);
        List<Reservation> reservations = resManager.getByTrip(t1);
        for (Reservation r : reservations) {
            resManager.delete(r);
        }
        em.remove(t1);
    }

    @Override
    public Trip update(Trip t) {
        em.merge(t);
        return t;
    }

    @Override
    public List<Trip> listAll() {
        List<Trip> list;
        Query query = em.createQuery("SELECT t FROM Trip t");
        list = (List<Trip>) query.getResultList();
        return list;
    }

    @Override
    public Trip findById(Long id) {
        Trip t = em.find(Trip.class, id);
        return t;
    }

    @Override
    public List<Trip> getByDestination(Destination d) {
        List<Trip> list;
        Query query = em.createQuery("SELECT t FROM Trip t WHERE t.destination = :d").setParameter("d", d);
        list = (List<Trip>) query.getResultList();
        return list;
    }

}
