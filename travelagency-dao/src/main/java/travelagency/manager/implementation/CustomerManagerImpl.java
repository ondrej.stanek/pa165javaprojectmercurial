package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.NoArgsConstructor;
import lombok.Setter;
import travelagency.entity.Customer;
import org.springframework.stereotype.Repository;
import travelagency.entity.Reservation;
import travelagency.entity.Role;
import travelagency.manager.CustomerManager;

/**
 * Implementation of customer manager
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class CustomerManagerImpl implements CustomerManager {

    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Customer create(Customer c) {
        if (c.getId() != null) {
            c.setId(null);
        }
        em.persist(c);
        return c;
    }

    @Override
    public void delete(Customer c) {
        Customer c1 = em.find(Customer.class, c.getId());
        ReservationManagerImpl resManager = new ReservationManagerImpl();
        resManager.setEm(em);
        List<Reservation> reservations = resManager.getByCustomer(c1);
        for (Reservation r : reservations) {
            resManager.delete(r);
        }
        em.remove(c1);
    }

    @Override
    public Customer update(Customer c) {
        if (c == null) {
            return null;
        }
        em.merge(c);
        return c;
    }

    @Override
    public List<Customer> listAll() {
        List<Customer> list;
        Query query = em.createQuery("SELECT c FROM Customer c");
        list = (List<Customer>) query.getResultList();
        return list;
    }

    @Override
    public Customer findById(Long id) {
        Customer c = em.find(Customer.class, id);
        return c;
    }

    @Override
    public List<Customer> getByUserRole(Role u) {
        List<Customer> list;
        Query query = em.createQuery("SELECT c FROM Customer c WHERE c.role = :u").setParameter("u", u);
        list = (List<Customer>)query.getResultList();
        return list;
    }
    
    @Override
    public List<Customer> getByEmail(String email) {
        List<Customer> list;
        Query query = em.createQuery("SELECT c FROM Customer c WHERE c.email = :e").setParameter("e", email);
        list = (List<Customer>)query.getResultList();
        return list;
    }
}
