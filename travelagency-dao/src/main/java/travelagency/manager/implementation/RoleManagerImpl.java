package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import travelagency.entity.Customer;
import travelagency.entity.Role;
import travelagency.manager.RoleManager;

/**
 * Implementation of user role manager
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class RoleManagerImpl implements RoleManager {
    
    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Role create(Role r) {
        if (r.getId() != null) {
            r.setId(null);
        }
        em.persist(r);
        return r;
    }

    @Override
    public void delete(Role r) {
        Role r1 = em.find(Role.class, r.getId());
        CustomerManagerImpl custManager = new CustomerManagerImpl();
        custManager.setEm(em);
        List<Customer> customers = custManager.getByUserRole(r1);
        for (Customer c : customers) {
            custManager.delete(c);
        }
        em.remove(r1);
    }

    @Override
    public Role update(Role r) {
        if (r == null) {
            return null;
        }
        em.merge(r);
        return r;
    }

    @Override
    public List<Role> listAll() {
        List<Role> list;
        Query query = em.createQuery("SELECT r FROM Role r");
        list = (List<Role>)query.getResultList();
        return list;
    }
    
    @Override
    public List<Role> getByRole(String role) {
        List<Role> list;
        Query query = em.createQuery("SELECT r FROM Role r WHERE r.role = :role").setParameter("role", role);
        list = (List<Role>)query.getResultList();
        return list;
    }
}
