package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import travelagency.entity.Excursion;
import javax.persistence.PersistenceContext;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;
import travelagency.manager.ExcursionManager;

/**
 * The implementation of excursion manager
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class ExcursionManagerImpl implements ExcursionManager {

    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Excursion create(Excursion e) {
        if (e.getId() != null) {
            e.setId(null);
        }
        em.persist(e);
        return e;
    }

    @Override
    public void delete(Excursion e) {
        Excursion e1 = em.find(Excursion.class, e.getId());
        for (Reservation r : e1.getReservations()) {
            r.deleteExcursion(e);
        }
        em.remove(e1);
    }

    @Override
    public Excursion update(Excursion e) {
        em.merge(e);
        return e;
    }

    @Override
    public List<Excursion> listAll() {
        List<Excursion> list;
        Query query = em.createQuery("SELECT e FROM Excursion e");
        list = (List<Excursion>) query.getResultList();
        return list;
    }

    @Override
    public Excursion findById(Long id) {
        Excursion e = em.find(Excursion.class, id);
        return e;
    }

    @Override
    public List<Excursion> getTripExcursions(Trip t) {
        List<Excursion> list;
        Query query = em.createQuery("SELECT e FROM Excursion e WHERE e.trip = :t").setParameter("t", t);
        list = (List<Excursion>) query.getResultList();
        return list;
    }
}
