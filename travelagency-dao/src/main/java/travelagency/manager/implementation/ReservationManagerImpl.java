package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import travelagency.entity.Customer;
import travelagency.entity.Excursion;
import travelagency.entity.Reservation;
import travelagency.entity.Trip;
import travelagency.manager.ReservationManager;

/**
 * The implementation of reservation manager
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class ReservationManagerImpl implements ReservationManager {

    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Reservation create(Reservation r) {
        if (r == null || r.getId() != null) {
            r.setId(null);
        }

        if (r.getExcursions() == null || r.getExcursions().isEmpty()) {
            em.persist(r);
        } else {
            em.merge(r);
        }
        return r;
    }

    @Override
    public void delete(Reservation r) {
        Reservation r1 = em.find(Reservation.class, r.getId());
        for (Excursion e : r1.getExcursions()) {
            e.deleteReservation(r);
        }
        em.remove(r1);
    }

    @Override
    public Reservation update(Reservation r) {
        em.merge(r);
        return r;
    }

    @Override
    public List<Reservation> listAll() {
        List<Reservation> list;
        Query query = em.createQuery("SELECT r FROM Reservation r");
        list = (List<Reservation>) query.getResultList();
        return list;
    }

    @Override
    public Reservation findById(Long id) {
        Reservation r = em.find(Reservation.class, id);
        return r;
    }

    @Override
    public List<Reservation> getByTrip(Trip t) {
        List<Reservation> list;
        Query query = em.createQuery("SELECT r FROM Reservation r WHERE r.trip = :t").setParameter("t", t);
        list = (List<Reservation>) query.getResultList();
        return list;
    }

    @Override
    public List<Reservation> getByCustomer(Customer c) {
        List<Reservation> list;
        Query query = em.createQuery("SELECT r FROM Reservation r WHERE r.customer = :c").setParameter("c", c);
        list = (List<Reservation>) query.getResultList();
        return list;
    }

    @Override
    public List<Reservation> findByString(String find) {
        List<Reservation> list;
        Query query = em.createQuery("SELECT r FROM Reservation r WHERE r.trip.name LIKE :find").setParameter("find", find);
        list = (List<Reservation>) query.getResultList();
        return list;
    }
}
