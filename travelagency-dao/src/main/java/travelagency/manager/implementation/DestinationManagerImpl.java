package travelagency.manager.implementation;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import travelagency.entity.Destination;
import javax.persistence.PersistenceContext;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Repository;
import travelagency.entity.Trip;
import travelagency.manager.DestinationManager;

/**
 * The implementation of destination manager
 *
 * @author Ondřej Staněk
 */
@Repository
@NoArgsConstructor
public class DestinationManagerImpl implements DestinationManager {

    @PersistenceContext
    @Setter
    private EntityManager em;

    @Override
    public Destination create(Destination d) {
        if (d.getId() != null) {
            d.setId(null);
        }
        em.persist(d);
        return d;
    }

    @Override
    public void delete(Destination d) {
        Destination d1 = em.find(Destination.class, d.getId());
        TripManagerImpl tripManager = new TripManagerImpl();
        tripManager.setEm(em);
        List<Trip> trips = tripManager.getByDestination(d);
        for (Trip t : trips) {
            tripManager.delete(t);
        }
        em.remove(d1);
    }

    @Override
    public Destination update(Destination d) {
        em.merge(d);
        return d;
    }

    @Override
    public List<Destination> listAll() {
        List<Destination> list;
        Query query = em.createQuery("SELECT d FROM Destination d");
        list = (List<Destination>) query.getResultList();
        return list;
    }

    @Override
    public Destination findById(Long id) {
        Destination d = em.find(Destination.class, id);
        return d;
    }
}
