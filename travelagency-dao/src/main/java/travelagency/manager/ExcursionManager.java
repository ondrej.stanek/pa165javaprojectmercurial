package travelagency.manager;

import java.util.List;
import travelagency.entity.Excursion;
import travelagency.entity.Trip;

/**
 * The interface of excursion manager
 *
 * @author Ondřej Staněk
 */
public interface ExcursionManager {

    /**
     * Creating of the excursion
     *
     * @param e excursion with attributes, if its id is not null, it sets it to
     * null
     * @return excursion created
     */
    Excursion create(Excursion e);

    /**
     * Deleting the excursion
     *
     * @param e excursion which is going to be deleted
     */
    void delete(Excursion e);

    /**
     * Updating the excursion
     *
     * @param e excursion which is going to be updated
     * @return excursion updated
     */
    Excursion update(Excursion e);

    /**
     * Make the list of all excursions in the system
     *
     * @return list of the excursions
     */
    List<Excursion> listAll();

    /**
     * Searching the excursion by id
     *
     * @param id the id of excursion, which is searched
     * @return excursion with this id
     */
    Excursion findById(Long id);

    /**
     * Make the list of excursions connected to trip t
     *
     * @param t the trip which contains excursions
     * @return the list of all excursions of the trip t
     */
    List<Excursion> getTripExcursions(Trip t);
}
