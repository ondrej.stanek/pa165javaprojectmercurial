package travelagency.manager;

import java.util.List;
import travelagency.entity.Destination;

/**
 * The interface of destination manager
 *
 * @author Ondřej Staněk
 */
public interface DestinationManager {

    /**
     * Creating of the destination
     *
     * @param d destination with attributes, if its id is not null, it sets it
     * to null
     * @return destination created
     */
    Destination create(Destination d);

    /**
     * Deleting the destination
     *
     * @param d destination which is going to be deleted
     */
    void delete(Destination d);

    /**
     * Updating the destination
     *
     * @param d destination which is going to be updated
     * @return destination updated
     */
    Destination update(Destination d);

    /**
     * Make the list of all destinations
     *
     * @return list of the destinations
     */
    List<Destination> listAll();

    /**
     * Searching the destination by id
     *
     * @param id the id of destination, which is searched
     * @return destination with this id
     */
    Destination findById(Long id);
}
