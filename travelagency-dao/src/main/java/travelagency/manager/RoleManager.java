package travelagency.manager;

import java.util.List;
import travelagency.entity.Role;

/**
 *
 * @author Ondřej Staněk
 */
public interface RoleManager {
    
    /**
     * Creating of the user role
     *
     * @param r user role with attributes, if its id is not null, it sets it to
     * null
     * @return user role created
     */
    Role create(Role r);

    /**
     * Deleting the user role
     *
     * @param r user role which is going to be deleted
     */
    void delete(Role r);

    /**
     * Updating the user role
     *
     * @param r user role which is going to be updated
     * @return user role updated
     */
    Role update(Role r);

    /**
     * Make the list of all user roles
     *
     * @return list of the user roles
     */
    List<Role> listAll();
    
    /**
     * Make the list of role to role name
     * 
     * @param role role name
     * @return the list of all role to role name
     */
    List<Role> getByRole(String role);
}
