package travelagency.manager;

import java.util.List;
import travelagency.entity.Destination;
import travelagency.entity.Trip;

/**
 * The interface of trip manager
 *
 * @author Ondřej Staněk
 */
public interface TripManager {

    /**
     * Creating of the trip
     *
     * @param t trip with attributes, if its id is not null, it sets it to null
     * @return trip created
     */
    Trip create(Trip t);

    /**
     * Deleting the trip with its excursions and reservations connected
     *
     * @param t trip which is going to be deleted
     */
    void delete(Trip t);

    /**
     * Updating the trip information
     *
     * @param t trip which is going to be updated
     * @return trip updated
     */
    Trip update(Trip t);

    /**
     * Make the list of all trips
     *
     * @return list of the trips
     */
    List<Trip> listAll();

    /**
     * Searching the trip by id
     *
     * @param id the id of trip, which is searched
     * @return trip with this id
     */
    Trip findById(Long id);

    /**
     * Make the list of trips to destination d
     *
     * @param d the destination
     * @return the list of all trips to destination d
     */
    List<Trip> getByDestination(Destination d);
}
