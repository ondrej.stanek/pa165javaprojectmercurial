package travelagency.manager;

import java.util.List;
import travelagency.entity.Customer;
import travelagency.entity.Role;

/**
 * The interface of customer manager
 *
 * @author Ondřej Staněk
 */
public interface CustomerManager {

    /**
     * Creating of the customer
     *
     * @param c customer with attributes, if its id is not null, it sets it to
     * null
     * @return c customer created
     */
    Customer create(Customer c);

    /**
     * Deleting the customer
     *
     * @param c customer who is going to be deleted
     */
    void delete(Customer c);

    /**
     * Updating the customer
     *
     * @param c customer who is going to be updated
     * @return customer updated
     */
    Customer update(Customer c);

    /**
     * Make the list of all customers
     *
     * @return list of the customers
     */
    List<Customer> listAll();

    /**
     * Searching the customer by id
     *
     * @param id the id of customer, who is searched
     * @return customer with this id
     */
    Customer findById(Long id);
    
    /**
     * Make the list of customers to user role u
     *
     * @param u the user role
     * @return the list of all customers to user role u
     */
    List<Customer> getByUserRole(Role u);
    
    /**
     * Make the list of customers to e-mail
     *
     * @param email the e-mail
     * @return the list of all customers to e-mail
     */
    List<Customer> getByEmail(String email);
}
